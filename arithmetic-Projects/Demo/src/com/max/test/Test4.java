package com.max.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Test4 {
	
	//获取不带扩展名的文件名
	 public static String getFileNameNoEx(String filename) { 
	        if ((filename != null) && (filename.length() > 0)) { 
	            int dot = filename.lastIndexOf('.'); 
	            if ((dot >-1) && (dot < (filename.length()))) { 
	                return filename.substring(0, dot); 
	            } 
	        } 
	        return filename; 
	    } 
	 
	 
//	 获取文件扩展名
	 public static String getExtensionName(String filename) { 
	        if ((filename != null) && (filename.length() > 0)) { 
	            int dot = filename.lastIndexOf('.'); 
	            if ((dot >-1) && (dot < (filename.length() - 1))) { 
	                return filename.substring(dot + 1); 
	            } 
	        } 
	        return filename; 
	    } 
	
	
	public static void main(String[] args) {

		//		从控制台读入一个文件名，判断该文件是否存在，如果该文件存在，则在原文件相同路径下创建一个文件名为"原文件名_copy"的新文件，
		//		该文件内容为原文件的拷贝，如果没有该文件不存在，则创建

		
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入文件名：");
        String path = sc.next();

        
        DataInputStream dis = null;
		DataOutputStream dos = null;

		try {
			//获取项目路径
			File directory = new File("");
	        String courseFile = directory.getCanonicalPath();
			
			String filepath = courseFile+"/"+path;
			File file = new File(filepath);
			if(!file.exists()) {
				// 创建文件
				try {
					file.createNewFile();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			String fileNameNoEx = getFileNameNoEx(filepath);
			String extensionName = getExtensionName(filepath);
			String filepath2 = fileNameNoEx+"_copy."+extensionName;
			File file2 = new File(filepath2);
			if(!file2.exists()) {
				// 创建文件
				try {
					file2.createNewFile();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			dis = new DataInputStream(new FileInputStream(filepath));
			dos = new DataOutputStream(new FileOutputStream(filepath2));

			byte arr [] = new byte[200];
			int line = 0;
			while ( (line = dis.read(arr)) != -1) {
				dos.write(arr);
			}
			dos.flush();
			System.out.println("拷贝成功");



		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(dis != null) {
				try {
					dis.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			if(dos != null) {
				try {
					dos.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
        

	}



}
