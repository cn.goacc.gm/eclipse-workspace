package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class BookInfo {
	private Long bookID;
	private String BookTitle;
	private BooKType typeId;
	private double price;
}
