package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class BooKType {
	private Long typeID;
	private String typeName;

}
