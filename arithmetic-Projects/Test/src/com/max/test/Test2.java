package com.max.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import com.max.domain.Student;

public class Test2 {

	public static void main(String[] args) {
		
		List<Student> list = new ArrayList<>();
		list.add(new Student("Lily", 90.0));
		list.add(new Student("Lulc", 60.0));
		list.add(new Student("Lilei", 30.0));
		list.add(new Student("Hanmeimei", 70.0));
		
		sort(list);

	}
	
	
	
	public static void sort(List<Student> list) {
		
		List<Student> sortList = new ArrayList<>();
		
		for (Student stu : list) {
			if(stu.getScore() >= 60 && stu.getScore() < 90) {
				sortList.add(stu);
			}
		}
		
		for (Student student : sortList) {
			System.out.println("等级为B的同学"+student);
		}
		
		sortList.sort(new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				return (int) (o1.getScore() - o2.getScore());
			}
		});
		
		for (Student student : sortList) {
			System.out.println("排序后的同学"+student);
		}
		
		
	}
	
	

}
