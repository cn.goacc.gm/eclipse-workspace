package com.max.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.max.domain.BooKType;
import com.max.domain.BookInfo;


//结果集封装
class MyResultHandle implements IResultSethandler<List<BookInfo>>{

	@Override
	public List<BookInfo> handler(ResultSet rs) throws Exception {
		
		List<BookInfo> list = new ArrayList<>();
		
		while(rs.next()) {
			BookInfo bookInfo = new BookInfo();
			bookInfo.setBookID(rs.getLong("bookID"));
			bookInfo.setBookTitle(rs.getString("BookTitle"));
			bookInfo.setPrice(rs.getDouble("price"));
			BooKType booKType = new BooKType();
			booKType.setTypeID(rs.getLong("typeId"));
			bookInfo.setTypeId(booKType);
			
			list.add(bookInfo);
		}
		
		return list;
	}
	
}



public class Test3 {
	
	public static void main(String[] args) throws Exception {
		
		//加载驱动
		Class.forName("com.mysql.jdbc.Driver");
		//获得对象工场
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Book?characterEncoding=utf8", "root", "li767787498");
		
		//连接对象
		Statement statement = con.createStatement();
		
		ResultSet resultSet = statement.executeQuery("select * from BookInfo where price>=50 and price<=100");
		List<BookInfo> list = new MyResultHandle().handler(resultSet);
		for (BookInfo bookInfo : list) {
			System.out.println("价格在50元到100元之间的图书："+bookInfo);
		}
		
		ResultSet sum = statement.executeQuery("select sum(price) from BookInfo as bi join BooKType as bt on bi.typeId = bt.typeID where typeName = '文学'");
		if(sum.next()) {
			int pricesum = sum.getInt(1);
			System.out.println("“文学”分类的图书的总价:"+pricesum);
		}
		
		
	}
	
	
	
	
	
	
	
	
	

}
