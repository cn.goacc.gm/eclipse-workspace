package com.max.ui;
import java.util.List;
import java.util.Scanner;

import com.max.domain.Employee;
import com.max.service.EmployeeService;

public class HomeUI {
	private static EmployeeService es = new EmployeeService();
	private static Employee overallem = null;  //全局的个人员工信息

	public static void main(String[] args) throws Exception {
		Home();
	}

	private static void Home() throws Exception {
		System.out.println("欢迎进入员工管理系统");
		System.out.println("请登录");
		login();
		home();
	}
	

	private static void home() throws Exception {
		boolean flag = true;
		while(flag) {
			System.out.println("请选择功能：1：查看所有员工信息  2：查询员工个人信息  3：添加新员工   4：修改员工信息   5：删除用户  6: 退出");
			Scanner sc = new Scanner(System.in);
			int select = sc.nextInt();
			switch (select) {
			case 1:
				getAll();
				break;
			case 2:
				queryUserInfo();
				break;
			case 3:
				add();
				break;
			case 4:
				flag = update(overallem);;
				break;
			case 5:
				delete();
				break;
			case 6:
				System.exit(0);
				break;
			default:
				System.out.println("参数错误，请重新输入");
				break;
			}
			
			if(!flag) {
				Home();
			}
			
		}
	}


	//删除用户
	private static void delete() {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入你要删除的用户的登录名：");
		String name = sc.next();
		System.out.println("请输入你要删除的用户的密码：");
		String password = sc.next();
		Employee deleteUser = es.checkUserNameAndPass(name,password);
		if(deleteUser != null) {
				System.out.println("编号\t姓名\t登录名\t年龄\t工资\t地址");
				System.out.println(deleteUser.getName()+ "\t" +deleteUser.getLoginName()+ "\t" +deleteUser.getAge()+ "\t" 
				+deleteUser.getSalary()+ "\t" +deleteUser.getAddress());
				
				System.out.println("确认你要删除的账号 输入ok确定删除");
				String select = sc.next();
				if(select.equals("ok") || select.equals("OK")) {
					es.delete(deleteUser);
					System.out.println("删除成功！");
				}else {
					System.out.println("删除失败");
				}
		}else {
			System.out.println("没有找到你要删除的用户信息：");
		}
		
		
	}

	//更新员工信息
	private static boolean update(Employee overallem2) throws Exception {
		Scanner sc = new Scanner(System.in);
		boolean flag = true;
		
		System.out.println("请选择你要修改的信息：1：名字  2：密码  3：年龄   4：工资   5：地址");
		Integer select = sc.nextInt();
		System.out.println("请输入你要修改的值：");
		
		
		switch (select.intValue()) {
		case 1: String value = sc.next(); es.update(overallem2, "Name", value); break;
		case 2: String value2 = sc.next(); es.update(overallem2, "Password", value2); flag = false; break;
		case 3: Integer value3  = sc.nextInt(); es.update(overallem2, "Age", value3); break;
		case 4: Integer value4 = sc.nextInt(); es.update(overallem2, "Salary", value4);break;
		case 5: String value5 = sc.next(); es.update(overallem2, "Address", value5);break;
		default:
			System.out.println("选择错误");
			break;
		}
		System.out.println("修改成功");
		return flag;
		
	}

	//添加用户信息
	private static void add() {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入姓名：");
		String name = sc.next();
		System.out.println("请输入登录名：");
		String loginName = sc.next();
		System.out.println("请输入密码：");
		String pass = sc.next();
		System.out.println("请输入年龄：");
		int age = sc.nextInt();
		System.out.println("请输入工资：");
		double salary = sc.nextInt();
		System.out.println("请输入地址：");
		String address = sc.next();
		es.addEmployee(name, loginName, pass, age, salary, address);
		System.out.println("添加成功");
	}
		
	private static void queryUserInfo() {
		System.out.println("请输入要查询的员工姓名（支持模糊查询）");
		Scanner sc = new Scanner(System.in);
		String name = sc.nextLine();
		List<Employee> list = es.queryUserInfo(name);
		System.out.println("编号\t姓名\t登录名\t年龄\t工资\t地址");
		for(int i = 0;i<list.size();i++) {
			Employee e = list.get(i);
			System.out.println(i +"\t"+ e.getName()+ "\t" +e.getLoginName()+ "\t" +e.getAge()+ "\t" +e.getSalary()+ "\t" +e.getAddress());
		}
	}


	private static void getAll() {
		System.out.println("编号\t姓名\t登录名\t年龄\t工资\t地址");
		List<Employee> list = es.getAllDate();
		for(int i = 0;i<list.size();i++) {
			Employee e = list.get(i);
			System.out.println(i +"\t"+ e.getName()+ "\t" +e.getLoginName()+ "\t" +e.getAge()+ "\t" +e.getSalary()+ "\t" +e.getAddress());
		}
	}




	private static void login() {
		while(true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("请输入用户名：");
			String loginName = sc.nextLine();
			System.out.println("请输入密码：");
			String pass = sc.nextLine();

			// 查询用户是否存在
			Employee em = es.checkUserNameAndPass(loginName, pass);
			if(em == null) {
				// 不存在
				System.out.println("用户名或密码错误，请重新输入");
			}else {
				// 存在
				System.out.println(em.getName() + "登录成功");
				overallem = em;
				break;
			}
		}
	}

}




