package com.max.dao;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.max.domain.Employee;
import com.max.utils.DBUtil;

public class EmployeeDao {

	private DBUtil db = new DBUtil();

	public List<Employee> getAllData() {

		List<Employee> list = new ArrayList<>();

		String sql = "select * from employee";
		ResultSet resultSet = db.query(sql);

		try {
			//			结果集的封装
			while(resultSet.next()) {
				Class class_ = Class.forName("com.max.domain.Employee");
				Object obj = class_.newInstance();

				BeanInfo beanInfo = Introspector.getBeanInfo(class_,Object.class);
				PropertyDescriptor[] psd = beanInfo.getPropertyDescriptors();
				for (PropertyDescriptor ps : psd) {
					Object val = resultSet.getObject(ps.getName());
					ps.getWriteMethod().invoke(obj, val);
				}
				list.add((Employee)obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void setAllDate(List<Employee> list) {

		String sql1 = "delete from employee";
		String sql2 = "insert into employee (name,loginName,password,age,salary,address) values(?,?,?,?,?,?)";
		db.execute(sql1);

		for (Employee e : list) {
			db.execute(sql2, e.getName(),e.getLoginName(), e.getPassword(),e.getAge(),e.getSalary(),e.getAddress());
		}

	}


}
