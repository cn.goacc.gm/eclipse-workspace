package com.max.service;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.max.dao.EmployeeDao;
import com.max.domain.Employee;

public class EmployeeService {
	
	private EmployeeDao employeeDao = new EmployeeDao();
	private List<Employee> list = employeeDao.getAllData();
	
	
	
	public Employee checkUserNameAndPass(String loginName, String pass) {
		for (Employee em : list) {
			if(em.getLoginName().equals(loginName) && em.getPassword().equals(pass)) {
				return em;
				
			}
		}
		
		return null;
	}



	public List<Employee> getAllDate() {
		return list;
	}



	public List<Employee> queryUserInfo(String name) {
		List<Employee> arList = new ArrayList<>();
		
		for (Employee employee : list) {
			if(employee.getName().contains(name)) {
				arList.add(employee);
				
			}
			
		}
		return arList;
	}



	public void addEmployee(String name, String loginName, String pass, int age, double salary, String address) {
		list.add(new Employee(name, loginName, pass, age, salary, address));

		employeeDao.setAllDate(list);
		
	}


	public <T> void update(Employee em,String property,T value) throws Exception {
		PropertyDescriptor pd = new PropertyDescriptor(property, em.getClass());
		Method method = pd.getWriteMethod();
		method.invoke(em, value);
		employeeDao.setAllDate(list);
			
		
	}
	
	public void delete(Employee employee) {
		list.remove(employee);
		employeeDao.setAllDate(list);
		
	}



	

}
