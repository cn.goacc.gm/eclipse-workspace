package com.max.text;

class MyThread2 extends Thread{
	private int beg;
	private int end;

	public MyThread2(int beg, int end) {
		super();
		this.beg = beg;
		this.end = end;
	}


	@Override
	public void run() {
		while(true){
			if(beg>end){
				return;
			}
			if(is(beg)){
				System.out.println(beg);
			}
			beg++;
		}

	}

	private boolean is(int beg2) {
		if(beg == 2){
			return true;
		}
		for(int i = 2 ;i<beg;i++){
			if((beg % i)==0 ){
				return false;
			}
		}
		return true;
	}

}



public class Test2 {
	public static void main(String[] args) {
		new MyThread2(1, 1000).start();
		new MyThread2(1001,2000).start();
		new MyThread2(2001,3000).start();

	}


}
