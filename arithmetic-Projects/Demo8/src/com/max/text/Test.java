package com.max.text;



class MyThread extends Thread{

	private int i ;
	
	public MyThread(int i) {
		this.i = i;
	}

	@Override
	public void run() {
		while(i >= 0) {
			System.out.println(Thread.currentThread().getName());
			i--;
		}
		
	}
	
}

public class Test {

	public static void main(String[] args) {

		MyThread my = new MyThread(200);
		
		Thread t1 = new Thread(my,"线程 1 正在运行");
		Thread t2 = new Thread(my,"线程 2 正在运行");
		
		t1.setPriority(10);
		t2.setPriority(6);
		
		t1.start();
		t2.start();
		
		
	}

}
