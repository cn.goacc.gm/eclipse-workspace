package com.max.text;

import java.io.IOException;
import java.nio.CharBuffer;

import javax.swing.plaf.SliderUI;

class ProducerIsConsumer {
	private int goods;
	
	public synchronized void factory() {
		while(goods == 5 ) {
			try {
				wait();  //当生产5个商品的时候 就停止生产
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		goods++;
		System.out.println("生产商品成功 ");
		notify();  	//不到5个继续生产
		
	}
	
	public synchronized void  consumer() {
		while(goods == 0) {
			try {
				wait();  //当没有商品了 停止消费
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		goods--;
		System.out.println("消费商品成功");
		notify();  //还有商品继续消费
		
	}
	
}

class MyProducer implements Runnable{

	private ProducerIsConsumer  producerIsConsumer;
	
	public MyProducer(ProducerIsConsumer producerIsConsumer) {
		super();
		this.producerIsConsumer = producerIsConsumer;
	}



	@Override
	public void run() {
		for(int i=0;i<10;i++) {
//			System.out.println("pro-- "+i);
				try {
					Thread.sleep(30);  //生产的快点
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			producerIsConsumer.factory();  //疯狂的生产
		}
		
		
	}
	
}

class MyConsumer implements Runnable{

	private ProducerIsConsumer  producerIsConsumer;
	
	public MyConsumer(ProducerIsConsumer producerIsConsumer) {
		super();
		this.producerIsConsumer = producerIsConsumer;
	}


	@Override
	public void run() {
		for(int i =0;i<10;i++) {
//			System.out.println("Con -- "+i);
				try {
					Thread.sleep(1000);   //不急着消费
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
			producerIsConsumer.consumer(); //疯狂的消费
			
		}
	}

	
}




public class Test3 {
	
	public static void main(String[] args) {
		 
		ProducerIsConsumer pis = new ProducerIsConsumer(); 
		
		//同一个对象放到两个线程里
		MyProducer myProducer = new MyProducer(pis);
		MyConsumer myConsumer = new MyConsumer(pis);
		
		Thread thread = new Thread(myProducer);
		Thread thread2 = new Thread(myConsumer);
		
		thread.start();
		thread2.start();
		
		
		
	}
	

}
