package com.max.test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.util.List;

import org.junit.Test;

import com.max.dao.UserDao;
import com.max.domain.Book;
import com.max.domain.ShoppingCart;
import com.max.domain.User;
import com.max.handler.BeanHandler;
import com.max.handler.BeanListHandler;
import com.max.service.UserService;
import com.max.utils.DBUtil;

public class test {
	private DBUtil db = new DBUtil();
	private BeanHandler<User> bh = new BeanHandler<>(User.class);
	private BeanListHandler<Book> blh = new BeanListHandler<>(Book.class);
	private UserDao ud = new UserDao();
	private UserService us = new UserService();

	@Test
	public void test1() throws Exception{
		DBUtil db = new DBUtil();
		ResultSet resultSet = db.query("select * from book");
		List<Book> list = blh.handler(resultSet);
		for (Book book : list) {
			System.out.println(book);
		}
	}
	
	@Test
	public void test2() throws Exception {
		
		String sql = "select * from user where username = ? and password = ?";
		ResultSet rs = db.query(sql, "user","123123");
		if (rs.next()) {
			// 创建一个对象
			Class<?> class1 = Class.forName("com.max.domain.User");
			Object obj = class1.newInstance();
			// 通过内省拿到属性
			BeanInfo info = Introspector.getBeanInfo(class1, Object.class);
			// 获取所有属性描述器
			PropertyDescriptor[] pds = info.getPropertyDescriptors();
			// 遍历
			for (PropertyDescriptor pd : pds) {
				// 获取属性名
				Object vla = rs.getObject(pd.getName());
				System.out.println(vla);
				// 获取set方法 并赋值
				pd.getWriteMethod().invoke(obj, vla);
			}
		}
		
		
	}
	
	@Test
	public void test3() {
		
		System.out.println(User.class);
		
	}
	
	@Test
	public void test4() throws Exception {
		ShoppingCart scd = ud.selectShoppingCartByUidAndBid("user", "s8989898");
		System.out.println(scd);
		
	}
	
	@Test
	public void test5() {
		
		String str = "A";
		System.out.println(str.toLowerCase());
	}
	
	
}
