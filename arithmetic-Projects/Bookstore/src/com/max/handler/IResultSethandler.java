package com.max.handler;

import java.sql.ResultSet;

//结果集处理器
public interface IResultSethandler<T> {
	T handler(ResultSet rs) throws Exception;
}
