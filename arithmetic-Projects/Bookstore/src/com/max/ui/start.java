package com.max.ui;

import java.util.List;
import java.util.Scanner;

import com.max.domain.Book;
import com.max.domain.ShoppingCart;
import com.max.domain.User;
import com.max.service.BookService;
import com.max.service.UserService;

public class start {
	
	private static User user = null;
	private static UserService us = new UserService();
	private static BookService bs = new BookService();
	
	
	//登录
	public static void login(){
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println(" ------ 登录-------");
			System.out.println("请输入用户名:");
			String username = sc.next();
			System.out.println("请输入密码：");
			String password = sc.next();
			user = us.getUserByIdAndPwd(username,password);
			if(user == null) {
				System.out.println("用户或密码错误，请重新登录");
				continue;
			}
			System.out.println(user.getUsername()+"登录成功!!!");
			MainUI();
			break;
		}
				
		
		
	}
	
	//注册
	public static void register() {
		Scanner sc = new Scanner(System.in);
		System.out.println(" ------注册-------");
		System.out.println("请输入用户名:");
		String username = sc.next();
		System.out.println("请输入密码：");
		String password = sc.next();
		System.out.println("请输入性别（0～1 0.女 1.男）：");
		Integer sex = sc.nextInt();
		System.out.println("请输入年龄:");
		Integer age = sc.nextInt();
		System.out.println("请输入地址");
		String address = sc.next();
		us.save(new User(username, password, sex, age, address));
		System.out.println("注册成功");
		login();
	}
	
	//主界面
	public static void MainUI() {
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println(" ------- 图书信息 ------ ");
			System.out.println("图书编号\t\t图书名\t\t价格");
			List<Book> booklist = bs.queryBookAll();
			for (Book book : booklist) {
				System.out.println(book.getId() +"\t"+ book.getName() +"\t\t"+ book.getPrice());
			}
			System.out.println("请选择购买的图书编号:");
			String bid = sc.next();
			System.out.println("请输入要购买的数量：");
			int number = sc.nextInt();
			
			ShoppingCart spc = us.isEqualBook(user.getUsername(),bid);
			if(spc != null) {
				number = spc.getNumber() + number;
				us.UpdataShoppingCart(spc.getId(),number);
			}else {
				us.saveShoppingCart(user.getUsername(),bid,number);
			}
			
			System.out.println("请选择:");
			System.out.println("1.继续购买 \t 2.到购物车结算");
			int select = sc.nextInt();
			if(select == 1) {
				continue;
			}
			
			//结算
			List<ShoppingCart> spcList = us.queryShoppingCartByUid(user.getUsername());
			System.out.println("已经购买的图书如下:");
			System.out.println("图书编号\t\t图书名\t\t数量");
			for (ShoppingCart s : spcList) {
				System.out.println(s.getBook().getId() +"\t\t"+ s.getBook().getName() +"\t\t"+ s.getNumber());
			}
			System.out.println("是否提交购物车：（y：提交  n：继续购买）");
			
			String select2 = sc.next();
			if(select2.toLowerCase().equals("y")) {
				us.deleteShoppingCartByUid(user.getUsername());
				System.out.println("购买成功！！");
				index();
				break;
			}
			
			
		}
		
		
		
	}
	
	
	public static void index() {
		Scanner sc = new Scanner(System.in);
		System.out.println("欢迎进入讯飞版网上书店请选择功能：1:注册2:登录");
		int select = sc.nextInt();
		switch (select) {
		case 1:
			register();
			break;
		case 2:
			login();
			break;
		default:
			System.out.println("输入错误，请重新选择");
			break;
		}
	}
	
	public static void main(String[] args) {
		index();
	}

}
