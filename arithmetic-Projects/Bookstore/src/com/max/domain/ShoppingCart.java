package com.max.domain;


public class ShoppingCart {
	private Integer id;
	private User user;
	private Book book;
	private Integer number;
	
	
	public ShoppingCart() {
		super();
	}


	public ShoppingCart(Integer id, User user, Book book, Integer number) {
		super();
		this.id = id;
		this.user = user;
		this.book = book;
		this.number = number;
	}


	@Override
	public String toString() {
		return "ShoppingCart [id=" + id + ", user=" + user + ", book=" + book + ", number=" + number + "]";
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Book getBook() {
		return book;
	}


	public void setBook(Book book) {
		this.book = book;
	}


	public Integer getNumber() {
		return number;
	}


	public void setNumber(Integer number) {
		this.number = number;
	}


	

	
	
	
}
