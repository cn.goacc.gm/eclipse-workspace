package com.max.utils;

import java.util.ResourceBundle;

public class Util {
	
	private static final String DBFILE_PATH = "resource/db";

	
	/**
	 * 读取数据库配置文件值
	 * @param key
	 * @return
	 */
	public static String getDBValue(String key) {
		ResourceBundle rb = ResourceBundle.getBundle(DBFILE_PATH);
		return rb.getString(key);
	}
	
}
