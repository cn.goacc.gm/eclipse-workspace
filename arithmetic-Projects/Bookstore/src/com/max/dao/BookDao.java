package com.max.dao;

import java.sql.ResultSet;
import java.util.List;

import com.max.domain.Book;
import com.max.domain.User;
import com.max.handler.BeanHandler;
import com.max.handler.BeanListHandler;
import com.max.utils.DBUtil;

public class BookDao {

	private DBUtil db = new DBUtil();
	private BeanHandler<Book> bh = new BeanHandler<>(Book.class);
	private BeanListHandler<Book> blh = new BeanListHandler<>(Book.class);
	
	
	public List<Book> selectBookAll() throws Exception {
		
		String sql = "select * from book";
		ResultSet resultSet = db.query(sql);
		List<Book> list = blh.handler(resultSet);
		return list;
		
	}
	
}
