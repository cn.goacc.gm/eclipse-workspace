package com.max.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.max.domain.Book;
import com.max.domain.ShoppingCart;
import com.max.domain.User;
import com.max.handler.BeanHandler;
import com.max.handler.IResultSethandler;
import com.max.utils.DBUtil;

public class UserDao {
	
	private DBUtil db = new DBUtil();
	private BeanHandler<User> bh = new BeanHandler<>(User.class);
	
	public void insert(User u) {
		String sql = "insert into user values(?,?,?,?,?)";
		db.execute(sql, u.getUsername(),u.getPassword(),u.getSex(),u.getAge(),u.getAddress());
	}

	public User selectByIdAndPwd(String username, String password) throws Exception {
		
		String sql = "select * from user where username = ? and password = ?";
		ResultSet resultSet = db.query(sql, username,password);
		User user = bh.handler(resultSet);
		
		return user;
	}

	public ShoppingCart selectShoppingCartByUidAndBid(String username, String bid) throws Exception {
		String sql = "select * from shoppingcart where uid = ? and bid = ?";
		ResultSet resultSet = db.query(sql, username,bid);
		ShoppingCart spc = new MyBeanHandler().handler(resultSet);
		return spc;
	}
	
	public void UpdateShoppingCart(Integer id, int number) {
		String sql = "update shoppingcart set number = ? where id = ?";
		db.execute(sql, number,id);
	}
	
	public void insertShoppingCart(String username, String bid, int number) {
		String sql = "insert into shoppingcart (uid,bid,number) values(?,?,?)";
		db.execute(sql, username,bid,number);
	}
	
	
	public List<ShoppingCart> selectAllShoppingCartByUid(String username) throws Exception {
		String sql = "select * from shoppingcart sc join book b on sc.bid = b.id where uid = ?";
		ResultSet resultSet = db.query(sql, username);
		List<ShoppingCart> list = new MyBeanListHandler().handler(resultSet);
		return list;
	}
	
	public void deleteShoppingCartByUid(String username) {
		String sql = "delete from shoppingcart where uid = ?";
		db.execute(sql,username);
		
	}
	
	
	//关联对象的结果集封装
	class MyBeanHandler implements IResultSethandler<ShoppingCart>{
			@Override
			public ShoppingCart handler(ResultSet rs) throws Exception {
				ShoppingCart spc = null;
				if(rs.next()) {
					User user = new User();
					Book book = new Book();
					int id = rs.getInt("id");
					int number = rs.getInt("number");
					user.setUsername(rs.getString("uid"));
					book.setId(rs.getString("bid"));
					spc = new ShoppingCart(id, user, book, number);
				}
				return spc;
			}
			
		}


		
	class MyBeanListHandler implements IResultSethandler<List<ShoppingCart>>{

		@Override
		public List<ShoppingCart> handler(ResultSet rs) throws Exception {
			List<ShoppingCart> shopList = new ArrayList<>();
			ShoppingCart spc = null;
			while (rs.next()) {
				User user = new User();
				Book book = new Book();
				int id = rs.getInt("id");
				int number = rs.getInt("number");
				
				user.setUsername(rs.getString("uid"));
				
				book.setId(rs.getString("bid"));
				book.setName(rs.getString("name"));
				book.setPrice(rs.getDouble("price"));
				
				spc = new ShoppingCart(id, user, book, number);
				
				shopList.add(spc);
			}
			
			return shopList;
		}

		
		
	}



	




		


		


		

}
