package com.max.service;

import java.util.List;

import com.max.dao.BookDao;
import com.max.domain.Book;

public class BookService {
	private BookDao bd = new BookDao();

	public List<Book> queryBookAll() {
		
		List<Book> bookAll = null;
		try {
			bookAll = bd.selectBookAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bookAll;
	}

}
