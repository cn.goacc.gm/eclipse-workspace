package com.max.service;

import java.util.List;

import com.max.dao.UserDao;
import com.max.domain.ShoppingCart;
import com.max.domain.User;

public class UserService {
	
	
	private UserDao ud = new UserDao();
	
	
	
	public void save(User user) {
		ud.insert(user);
	}
	
	
	public User getUserByIdAndPwd(String username, String password){
		User user = null;
		try {
			user = ud.selectByIdAndPwd(username,password);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return user;
	}


	public ShoppingCart isEqualBook(String username, String bid) {
		ShoppingCart spc = null;
		try {
			spc = ud.selectShoppingCartByUidAndBid(username,bid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return spc;
	}


	public void UpdataShoppingCart(Integer id, int number) {
		
		ud.UpdateShoppingCart(id,number);
		
	}


	public void saveShoppingCart(String username, String bid, int number) {
		ud.insertShoppingCart(username,bid,number);
		
	}


	public List<ShoppingCart> queryShoppingCartByUid(String username) {
		List<ShoppingCart> list = null;
		try {
			list = ud.selectAllShoppingCartByUid(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}


	public void deleteShoppingCartByUid(String username) {

		ud.deleteShoppingCartByUid(username);
		
	}


	
	
	
	
	
	
	

}
