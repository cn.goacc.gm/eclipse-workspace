package com.max.dao.impl;

import com.max.dao.GoodsDao;
import com.max.domain.Goods;

public class GoodsDaoImpl implements GoodsDao{

	@Override
	public void save(Goods goods) {
		System.out.println("商品保存到数据库"+goods);
		
	}

	@Override
	public void save(Goods goods, String str) {
		System.out.println("商品保存到数据库"+goods +"  方法重载"+str);
		
	}

}
