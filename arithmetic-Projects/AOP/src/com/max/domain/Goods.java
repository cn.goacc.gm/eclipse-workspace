package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class Goods {
	private Long gid;
	private String gname;
}
