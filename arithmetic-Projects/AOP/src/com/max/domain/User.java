package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString
public class User {
	private Long uid;
	private String name;

}
