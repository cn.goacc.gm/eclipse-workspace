package com.max.proxy;

import java.lang.reflect.Method;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import com.max.dao.UserDao;



public class UserCglibProxy {

	public UserDao CreateCglibProxy(UserDao userDao) {
		
		//创建核心类
		Enhancer enhancer = new Enhancer();
		//设置父类 （创建子类 自动继承UserDao）
		enhancer.setSuperclass(userDao.getClass());
		//设置回调
		enhancer.setCallback(new MethodInterceptor() {
			
			@Override
			public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
//				MethodProxy (代理的方法)
				System.out.println("日志打印");
				return methodProxy.invokeSuper(o, args);
			}
		});
		//创建代理对象 并将创建的子类对象返回给你
		UserDao UserDaoProxy = (UserDao) enhancer.create();
		return UserDaoProxy;
		
	}
	

}
