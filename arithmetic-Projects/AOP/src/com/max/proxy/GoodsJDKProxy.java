package com.max.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;


import com.max.dao.GoodsDao;

public class GoodsJDKProxy {

	// 产生代理的方法
	public GoodsDao createProxy(GoodsDao goodsDao) {
		
		// 参数 ： 类加载器  ， 接口数组 ， 处理者   返回值：代理对象
			GoodsDao GoodsDaoProxy = (GoodsDao) Proxy.newProxyInstance(goodsDao.getClass().getClassLoader(), goodsDao.getClass().getInterfaces(), new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				System.out.println("日志打印");
				return method.invoke(goodsDao, args);
			}
		});
		
			//返回代理对象
		return GoodsDaoProxy;
	}
	
	
	

}

