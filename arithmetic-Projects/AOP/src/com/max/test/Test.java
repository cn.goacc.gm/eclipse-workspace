package com.max.test;

import com.max.dao.GoodsDao;
import com.max.dao.UserDao;
import com.max.dao.impl.GoodsDaoImpl;
import com.max.domain.Goods;
import com.max.domain.User;
import com.max.proxy.GoodsJDKProxy;
import com.max.proxy.UserCglibProxy;

public class Test {

	public static void t1() {
		GoodsDao goodsDao = new GoodsDaoImpl();
		GoodsDao createProxy = new GoodsJDKProxy().createProxy(goodsDao);
		Goods goods = new Goods();
		goods.setGid(1L);
		goods.setGname("商品名称");

		createProxy.save(goods,"重载-----");

	}

	public static void test2() {
		UserDao userDao = new UserDao();
		UserDao proxy = new UserCglibProxy().CreateCglibProxy(userDao);
		User user = new User();
		user.setUid(1L);
		user.setName("Name");
		proxy.save(user);

	}		


	public static void main(String[] args) {
		t1();

	}


}
