package com.max.main;

import java.util.Arrays;

public class Test8 {
	public static void main(String[] args) {
//		5：有如下数组：int  arr[]  =  {12,43,66,56,87,62,98};,请找出其中最大值和最小值，然后把剩下元素按照升序存放在新数组里
		int  arr[]  =  {12,43,66,56,87,62,98};
		
		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<=i;j++) {
				if(arr[i]>=arr[j]) {
					int t=arr[i];
					arr[i]=arr[j];
					arr[j]=t;
				}
			}
		}
		
		int max=arr[0];
		int min=arr[arr.length-1];
//		[98, 87, 66, 62, 56, 43, 12]
		
		int[] newArr=new int[arr.length-2];
		int m=0;
		for(int x=1;x<arr.length-1;x++) {
			newArr[m]=arr[x];
			m++;
		}
		System.out.println("max="+max+"  min="+min);
		System.out.println("剩下元素："+Arrays.toString(newArr));
		
		
	}

}
