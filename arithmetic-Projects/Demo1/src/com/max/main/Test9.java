package com.max.main;

import java.util.Arrays;

public class Test9 {

	public static void main(String[] args) {
		

//	6.	有如下数组: int arr[]= 					{12,43,66,98,56,87,62)};
//	要求1:请找出其中最大值，然后删了，最终数组为		{12,43,66,56,87,62}
//	要求2:请在最大值的前面插入一个元素为2，最终数组为:" {12,43,66,2,98,56,87,62}
//												  0  1  2  3 4  5  6  7
		
		
		int arr[]= {12,43,66,98,56,87,62};
		int[] newArr=new int[arr.length-1];
		int m=0;
		
//		1.
		int max=arr[0];
		int MaxIndex=0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i]>max) {
				max=arr[i];
				MaxIndex=i;
			}
		}
		
		for(int x=0;x<arr.length;x++) {
			if(arr[x] != max) {
				newArr[m]=arr[x];
				m++;
			} 
		}
		arr=newArr;
		System.out.println(Arrays.toString(arr));
		
//		2
		int[] new2Arr = new int[arr.length+2];
		int c=0;
		new2Arr[MaxIndex]=2;
		new2Arr[MaxIndex+1]=max;
		for(int n=0;n<new2Arr.length;n++) {
			if(n == MaxIndex || n == MaxIndex+1) {
				continue;
			}
			System.out.println(n);
			new2Arr[n]=arr[c];
			c++;
		}
		arr=new2Arr;
		
		System.out.println(Arrays.toString(arr));
		
		
	}
}
