package com.max.main;

public class Test4 {
	public static void main(String[] args) {

//		本金10000元存入银行，年利率是千分之三，每过1
//		年，将本金和利息相加作为新的本金。计算5年后，获
//		得的本金是多少
		
		//5年,遍历5年的钱数
		double ben = 10000;
		for (int i = 1; i <= 5; i++) {
			ben = (double) (ben * 1.003); //（电脑默认数据类型为int，所以这里需要强制转换）
		}
		System.out.println("5年之后一共取出:" + ben + "元");

	}
}
