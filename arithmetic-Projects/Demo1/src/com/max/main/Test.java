package com.max.main;

import java.util.Arrays;

public class Test {
	public static void main(String[] args) {

		int[] a = {4,3,8,1,9};

		for(int i=0;i<a.length;i++) {
			for(int j=0;j<=i;j++) {
				if(a[i]<=a[j]) {
					int t = a[i];
					a[i]=a[j];
					a[j]=t;
				}
			}
		}
		
		System.out.println(Arrays.toString(a));
	}		
}

