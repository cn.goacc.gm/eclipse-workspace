package com.max.service;

import java.util.List;

import com.max.dao.UserDao;
import com.max.domain.User;

public class UserService {
	private UserDao userDao = new UserDao();
	
	
	private List<User> list = userDao.getAllUserData();
	
	
	
	public boolean isUserNumber(String userNumber) {
		
		for (User user : list) {
			if(user.getUserNumber().equals(userNumber)) {
				return false;
			}
		}
		
		return true;
		
	}



	public void login_User(User user) {
		
		
		list.add(user);

	}



	public User isLogin(String userNumber, String password) {
		
		for (User user : list) {
			if(user.getUserNumber().equals(userNumber) &&  user.getPassword().equals(password)) {
				return user;
			}
		}
		
		return null;
	}



	public void updatePassword(User user, String password) {
		user.setPassword(password);
		
	}
	
	
	
	
	

}
