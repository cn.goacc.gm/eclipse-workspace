package com.max.service;

import java.util.List;

import com.max.dao.FoodDao;
import com.max.dao.UserDao;
import com.max.domain.Food;
import com.max.domain.User;

public class FoodService {
	
	private FoodDao foodDao = new FoodDao();
	
	
	private List<Food> list = foodDao.getAllFoodData();
	
	
	public List<Food> getAllFoodData(){
		
		return list;
		
	}


	public boolean idFoodId(Integer foodID) {
		int[] id = foodDao.getFoodId();
		for (int i : id) {
			if(foodID.intValue() == i) {
				return false;
			}
		}
		return true;
	}
	
	
	

}
