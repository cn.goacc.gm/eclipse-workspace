package com.max.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.max.dao.FoodDao;
import com.max.domain.Food;
import com.max.domain.User;
import com.max.service.FoodService;
import com.max.service.UserService;

public class HomeUI {
	
	
	public static void StartUI() {
		System.out.println("          蚌埠电子学院订餐系统");
		System.out.println("欢迎进入蚌埠电子学院订餐系统，请选择1：登录  2：注册");
		Scanner sc = new Scanner(System.in);
		Integer select = sc.nextInt();
		switch(select) {
			case 1: login();break;
			case 2: newUser();break;
		}
		
	}
	
	
	//登录
	public static void login() {
		UserService userService = new UserService();
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("请登录：");
			System.out.println("请输入用户名：");
			String UserNumber = sc.nextLine();
			System.out.println("请输入密码：");
			String Password = sc.nextLine();
			User user = userService.isLogin(UserNumber,Password);
			if(user!=null) {
				System.out.println(" 登录成功 ");
				index(user);
				break;
			}
			System.out.println("用户名或密码错误  ");
		}
		
		
		
	}
	
	//注册
	public static void newUser() {
		Scanner sc = new Scanner(System.in);
		
		UserService userService = new UserService();
		
		System.out.println("欢迎注册信息：");
		String UserNumber = "";
		while(true) {
			System.out.println("请输入用户名：");
			UserNumber = sc.nextLine();
			boolean isnum = userService.isUserNumber(UserNumber);
			if(isnum) {break;}else { System.out.println("用户名已存在，换个试试吧！");}
			
		}
		
		System.out.println("请输入密码：");
		String Password = sc.nextLine();
		System.out.println("请输入姓名：");
		String UserName = sc.nextLine();
		System.out.println("请输入院系：");
		String faculty = sc.nextLine();
		User user = new User();
		user.setUserNumber(UserNumber);
		user.setPassword(Password);
		user.setUserName(UserName);
		user.setFaculty(faculty);
		user.setFoodList(new ArrayList<Food>());
		
		userService.login_User(user);
		System.out.println("注册成功！");
		login();
		
		
	}
	
	//主页
	public static void index(User user) {
		Scanner sc = new Scanner(System.in);
		System.out.println("1：订餐  2：查看个人订餐   3：修改个人信息    4：退出");
		boolean flag = true;
		while(flag) {
			Integer select = sc.nextInt();
			switch(select) {
				case 1: orderFood(user); flag = false;break;
				case 2: checkUser(user); flag = false; break;
				case 3: updateUser(user); flag = false;break;
				case 4:System.exit(0);
			}
		}
		
	}
	
	//订餐
	public static void orderFood(User user) {
		Scanner sc = new Scanner(System.in);
		System.out.println("查看个人订餐");
		FoodService foodService = new FoodService();
		List<Food> allFoodData = foodService.getAllFoodData();
		
		System.out.println("编号	 名称		价格");
		for (Food food : allFoodData) {
			System.out.print(food.getId());
			System.out.print("     "+food.getFoodName());
			System.out.print("     "+food.getPrice());
			System.out.println();
		}
		
		boolean flag  = true;
		Integer foodID = null;
		while(flag) {
			System.out.println("请输入订餐的编号:");
			foodID = sc.nextInt();
			flag = foodService.idFoodId(foodID);
			if(flag) {
				System.out.println("商品不存在，请重新输入。。。");
			}
		}
			System.out.println("请输入订餐的数量:");
			Integer num = sc.nextInt();
			for(int i = 0;i<num.intValue();i++) {
				user.getFoodList().add(allFoodData.get(foodID));
			}	
			System.out.println("购买成功");
			index(user);
		
		
		
	} 
	//查看个人订餐
	public static void checkUser(User user){
		
		double sum = 0;
		for (Food food : user.getFoodList()) {
			System.out.println(food);
			sum+=food.getPrice();
		}
		System.out.println("总计:"+sum);
		index(user);
		
		
	}
	//修改个人信息
	public static void updateUser(User user){
		Scanner sc = new Scanner(System.in);
		UserService userService = new UserService();
		
		System.out.println("请确认个人信息");
		System.out.println("用户名:"+user.getUserNumber());
		System.out.println("姓名:"+user.getUserName());
		System.out.println("院系:"+user.getFaculty());
		System.out.println("请输入要修改的密码：");
		String password = sc.nextLine();
		userService.updatePassword(user,password);
		System.out.println("修改成功！ 请重新登录 ");
		login();
		
		
	}
	
	
	
	public static void main(String[] args) {
		//修改了 点菜时，如果选择的菜的id 不存在会报错
		//修改了 多次点餐 上次点餐信息丢失的问题
		
		StartUI();
		
		
	}

}
