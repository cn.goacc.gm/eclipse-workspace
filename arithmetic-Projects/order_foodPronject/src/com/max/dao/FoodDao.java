package com.max.dao;

import java.util.ArrayList;
import java.util.List;

import com.max.domain.Food;
import com.max.domain.User;

public class FoodDao {
	
	private static List<Food> list =new ArrayList<>();
	
	
	static {
		Food food = new Food(1,"回锅肉",18);
		Food food2 = new Food(2,"牛肉炒饭",15);
		Food food3 = new Food(3,"青椒肉丝盖饭",16);
		Food food4 = new Food(4,"牛肉汤",15);
		list.add(food);
		list.add(food2);
		list.add(food3);
		list.add(food4);
	}
	
	
	public List<Food> getAllFoodData(){
		
		return list;
	}
	
	//获取所以食物编号
	public int[] getFoodId() {
		int[] id = new int[list.size()];
		int i = 0;
		for (Food food : list) {
			id[i]=food.getId().intValue();
			i++;
		}
			
		return id;
			
	}
	
	

}
