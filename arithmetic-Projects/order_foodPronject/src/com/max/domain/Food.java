package com.max.domain;

public class Food {
	private Integer id;
	private String FoodName;
	private double Price;
	
	
	public Food() {
		super();
	}


	public Food(Integer id, String foodName, double price) {
		super();
		this.id = id;
		FoodName = foodName;
		Price = price;
	}


	@Override
	public String toString() {
		return "Food [id=" + id + ", FoodName=" + FoodName + ", Price=" + Price + "]";
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFoodName() {
		return FoodName;
	}
	public void setFoodName(String foodName) {
		FoodName = foodName;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(Double price) {
		Price = price;
	}
	
	
	
}
