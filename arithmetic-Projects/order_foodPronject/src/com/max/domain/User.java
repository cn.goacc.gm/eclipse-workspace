package com.max.domain;

import java.util.List;

public class User {
	private String UserNumber;
	private String Password;
	private String UserName;
	private String faculty;
	private List<Food> FoodList;
	
	
	
	public User(String userNumber, String password, String userName, String faculty, List<Food> foodList) {
		super();
		UserNumber = userNumber;
		Password = password;
		UserName = userName;
		this.faculty = faculty;
		FoodList = foodList;
	}

	
	public User() {
		super();
	}



	@Override
	public String toString() {
		return "User [UserNumber=" + UserNumber + ", Password=" + Password + ", UserName=" + UserName + ", faculty="
				+ faculty + ", FoodList=" + FoodList + "]";
	}
	
	
	
	public String getUserNumber() {
		return UserNumber;
	}
	public void setUserNumber(String userNumber) {
		UserNumber = userNumber;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getFaculty() {
		return faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	public List<Food> getFoodList() {
		return FoodList;
	}
	public void setFoodList(List<Food> foodList) {
		FoodList = foodList;
	}
	
	
	

}
