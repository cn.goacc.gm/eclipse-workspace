package com.max.demo2;

public class Student {
	private String UserName;
	private String Sex;
	private Integer age;
	
	
	@Override
	public String toString() {
		return "Student [UserName=" + UserName + ", Sex=" + Sex + ", age=" + age + "]";
	}
	
	
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	

}
