package com.max.demo2;

public class StudentService {
	
	private StudentDao studentDao  = new StudentDao();
	
	private Student student = studentDao.getAllData();
	
	
	public void SetStuAge(Integer age) {
		
		student.setAge(age);
		System.out.println(" service Student ---  "+student.getAge());
	}
	
	public Integer GetStuAge() {
		
		return student.getAge();
		
	}

}
