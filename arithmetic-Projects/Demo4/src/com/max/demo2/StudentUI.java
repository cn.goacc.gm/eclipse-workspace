package com.max.demo2;

public class StudentUI {
	
	
	public static void UI1() {
		StudentService stuService = new StudentService();
		UI2(stuService);
		UI3(stuService);
		
	}
	
	public static void UI2(StudentService service) {
		service.SetStuAge(100);
	}
	public static void UI3(StudentService service) {
		Integer getStuAge = service.GetStuAge();
		System.out.println(getStuAge);
	}
	
	
	public static void main(String[] args) {
		
		UI1();
		
		
	}

}
