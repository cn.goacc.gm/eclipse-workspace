package com.max.demo1;

import java.util.Arrays;

public class Test {
	public static void main(String[] args) {
		
//		1.创建字符串str"IhLeOllVoEwJorAlVdA"。
//		2.创新两个新的字符串newstr1和newstr2，将str中的大写字母放到newstr1中，小写字母放到newstr2中，组成两个新的字符串。
//		3.将newstr1中的大写字母全部转换为小写字母并输出，newstr2中的小写字母全都转换成大写字母并输出。(判断是否是大小写字母可以通过ascii，
//		或者通过Character类有方法可以直接使用)
		
		String str = "IhLeOllVoEwJorAlVdA";
		char[] charArray = str.toCharArray();
		String newstr1 = "";
		String newstr2 = "";
		for (char c : charArray) {
	        if ((char)c>='a'&&(char)c<='z') {
	        	newstr2 = newstr2 + c;
	        }else {
	        	newstr1 = newstr1 + c;
	        }
	    }
		System.out.println(newstr1);
		System.out.println(newstr2);
		System.out.println(newstr1.toLowerCase());
		System.out.println(newstr2.toUpperCase());
		
	}

}
