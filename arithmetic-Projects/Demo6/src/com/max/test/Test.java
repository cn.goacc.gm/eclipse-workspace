package com.max.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Test {
	public static void main(String[] args) throws Exception {

		DataInputStream dis = null;
		DataOutputStream dos = null;
		
		try {
			File file = new File("");
			String Path = file.getCanonicalPath();

			String SourcePath = Path+"/source/Img.png";
			String goalPath = Path+"/goal/Img2.png";

			dis = new DataInputStream(new FileInputStream(SourcePath));
			dos = new DataOutputStream(new FileOutputStream(goalPath));

			byte[] buf = new byte[1024];        
			int bytesRead;        
			
			while ((bytesRead = dis.read(buf)) != -1) {
				dos.write(buf, 0, bytesRead);
			}

		}finally {
			dis.close();
			dos.close();
		}


	}


}
