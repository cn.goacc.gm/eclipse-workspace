package com.max.test;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.iflytek.test2.Student;

public class Test {



	public static void main(String[] args) throws Exception {

		List<Student> list = new ArrayList<>();

		SAXReader read = new SAXReader();
		Document dom = read.read("src/com/max/test/test1.xml");

		//获取根标签
		Element rootEom = dom.getRootElement();
		Iterator iterator = rootEom.elementIterator();

		while (iterator.hasNext()) {
			Element el = (Element) iterator.next();
			String class_ = el.attributeValue("class");
			Object obj = Class.forName(class_).newInstance();
			Iterator el2 = el.elementIterator();


			while (el2.hasNext()) {
				Element elm = (Element) el2.next();
				String name = elm.attributeValue("name");
				String text = elm.getStringValue();
				//内省
				PropertyDescriptor pd = new PropertyDescriptor(name,obj.getClass());
				Method method = pd.getWriteMethod();
				method.invoke(obj, text);
			}
			list.add((Student)obj);
		}
		for (Student student : list) {
			System.out.println(student);
		}



	}

}
