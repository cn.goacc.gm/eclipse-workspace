package com.max.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Test2 {
	public static void main(String[] args) throws Exception {

		List<Student> stulist = new ArrayList<>();
		stulist.add(new Student("hanlu", 21));
		stulist.add(new Student("rongyu", 22));
		stulist.add(new Student("wangjie", 21));
		stulist.add(new Student("zhangsan", 30));
		stulist.add(new Student("lisi", 20));
		for (Student student : stulist) {
			System.out.println("未排序"+student);
		}

		Collections.sort(stulist, new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				int one = ((Student) o1).getAge();
				int two = ((Student) o2).getAge();
				return one-two;
			}
		});

		System.out.println();
		for (Student student : stulist) {
			System.out.println("排序后"+student);
		}
		
		File directory = new File("");  //获取项目路径
        String courseFile = directory.getCanonicalPath();
        courseFile = courseFile+"/StudentInfo.txt";
		
		writerDemo(courseFile,stulist);
		
		

	}


	private static void writerDemo(String fileName,List<Student> list) {
		Writer w = null;
		try {

			File file = new File(fileName);
			if(!file.exists()) {
				file.createNewFile();
			}
			w = new FileWriter(file);
			for (Student student : list) {
				w.write(student.getName()+",");
				w.write(""+student.getAge());
				w.write("\r\n");
			}
			
			w.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if(w != null) {
				try {
					w.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}

		}
	}



}
