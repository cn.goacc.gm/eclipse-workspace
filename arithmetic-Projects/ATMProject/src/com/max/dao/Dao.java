package com.max.dao;

import com.max.domain.User;

public class Dao {
	
	private static User user = new User();
	
	static {
		user.setUsername("张三");
		user.setBankcode("123456");
		user.setPassword("0000000");
		user.setMoney(10000);
		
	}
	
	public static User getUserData() {
		
		return user;
	}

}
