package com.max.domain;

public class User {
	private String Username;
	private String Bankcode;
	private String password;
	private double money;
	
	
	@Override
	public String toString() {
		return "User [Username=" + Username + ", Bankcode=" + Bankcode + ", password=" + password + ", money=" + money
				+ "]";
	}
	
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getBankcode() {
		return Bankcode;
	}
	public void setBankcode(String bankcode) {
		Bankcode = bankcode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	
	

}
