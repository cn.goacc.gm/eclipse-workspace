package com.max.ui;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.max.dao.Dao;
import com.max.domain.User;
import com.max.service.ATMService;

public class home {
//菜单
	public static void menu() {
		System.out.println("--------------------");
		System.out.println("欢迎使用模拟自动取款机程序");
		System.out.println("--------------------");
		String[] text = new String[] {"取款","查询信息","密码设置","退出系统"};
		for(int i=0;i<text.length;i++) {
			System.out.println(i+1+".>"+text[i]);
		}
	}
	
//账号判断
	public static void StartUI() {
		Scanner sc = new Scanner(System.in);
		menu();
		System.out.println("请输入你的银行卡号:");
		
//		正则
		Pattern pattern = Pattern.compile("^\\d{5,11}$");
		ATMService service = new ATMService();
		int i = 3;
		boolean flag = true;
		String Code = null;
		while(i>0) {
			if(flag) {
				Code =  sc.nextLine();
				Matcher matcher = pattern.matcher(Code);
				if(!matcher.matches()){
					System.out.println("请输入格式正确的银行卡号");
					continue;
				}
			}
			flag = false;
			System.out.println("请输入该卡号密码:");
			String psw = sc.nextLine();
			boolean is = service.isClientele(Code, psw);
			if(is) {
//				登录成功
				System.out.println("登录成功 - 请选择您要操作的项目（1-4）");
				MenuSkip();
				i=-1;
			}
			if(i > 1) {
				System.out.println("输入错误的卡号密码 还有"+(i-1)+"机会");
			}
			i--;
		}
		if(i==0) {
			System.out.println("对不起 卡已被没收 ");}
	}
	
//跳转中转站
	public static void MenuSkip() {
		menu();
		Scanner sc = new Scanner(System.in);
		boolean flag =true;
		ATMService atmService = new ATMService();
		while (flag) {
			String bool = sc.nextLine();
			Matcher matcher = Pattern.compile("[0-9]*").matcher(bool);
			if(matcher.matches() && Integer.valueOf(bool) >0 && Integer.valueOf(bool) <5) {
				Integer boolint = Integer.valueOf(bool);
				switch(boolint) {
					case 1: withdrawal(atmService);break;
					case 2: query(atmService); break;
					case 3: setPassword_(atmService); flag = false; 
					System.out.println("请重新登录 --- "); StartUI();break;
					case 4: System.exit(0);break;
				}
			}
			
			if(flag) {
				System.out.println("请输入（1-4）之间的数字");
				}
		}
		
	}
	
//取款
	public static void  withdrawal(ATMService atmService) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入你要取款的金额:");
		double money = sc.nextInt();
		double Remoney = atmService.wdService(money);
		if(Remoney == -1) {
			System.out.println("余额不足");
		}else {
			System.out.println("取款成功 剩余："+Remoney);
		}
	}
//查询
	public static void query(ATMService atmService) {
		User allData = atmService.getAllData();
		System.out.println("用户名："+allData.getUsername());
		System.out.println("银行卡号："+allData.getBankcode());
		System.out.println("剩余金额："+allData.getMoney());
	}
//密码设置
	public static void setPassword_(ATMService atmService) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入你要修改的密码:");
		String psd = sc.nextLine();
		atmService.setPassword(psd);
		System.out.println("修改成功");
		
	}
	
	public static void main(String[] args) {
		StartUI();
	
	}
}
