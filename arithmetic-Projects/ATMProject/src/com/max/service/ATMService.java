package com.max.service;

import java.util.Arrays;

import com.max.dao.Dao;
import com.max.domain.User;

public class ATMService {
	private Dao dao = new Dao();
	private User user = dao.getUserData();
	
	public boolean isClientele(String code,String password) {
		if(user.getBankcode().equals(code) && user.getPassword().equals(password)) {
			return true;
		}
		return false;
		
	}
	
	public double wdService(Double _money) {
		
		double money = user.getMoney();
		money = money - _money;
		if(money<-1) {
			return -1;
		}
		
		user.setMoney(money);
		
		return money;
		
	}
	public User getAllData() {
		
		return user;
	}
	
	public void setPassword(String newpassword) {
			user.setPassword(newpassword);
	}
	
}

