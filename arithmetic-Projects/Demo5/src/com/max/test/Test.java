package com.max.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		
//		1：利用Map，完成下面的功能：从命令行读入一个字符串，表示一个年份，输出该年的世界杯冠军是哪支球队。
//		如果该年没有举办世界杯，则输出：没有举办世界杯。世界杯冠军以及对应的夺冠年份，请参考网上
		
		Scanner sc = new Scanner(System.in);
		
		Map<String,String> map = new HashMap<>( );
		map.put( "2014","德国" );
		map.put( "2010","西班牙" );
		map.put( "2006","意大利" );
		map.put( "2002","巴西" );
		map.put( "1998","法国" );
		map.put( "1994","巴西" );
		map.put( "1990","德国" );
		map.put( "1986","阿根廷" );
		map.put( "1982","意大利" );
		map.put( "1978","阿根廷" );
		map.put( "1974","德国" );
		map.put( "1970","巴西" );
		map.put( "1966","英格兰" );
		map.put( "1962","巴西" );
		map.put( "1958","巴西" );
		map.put( "1954","德国" );
		map.put( "1950","乌克兰" );
		map.put( "1938","意大利" );
		map.put( "1934","意大利" );
		map.put( "1930","乌克兰" );
		System.out.println(" 请输入年份：");
		String str = sc.next();
		for (String key : map.keySet()) {
			if(str.equals(key)) {
				System.out.println(key+"年夺冠的是"+map.get(key));
				return ;
			}
		}
		System.out.println("没有举办世界杯");
		
		
		
	}
	
	

}
