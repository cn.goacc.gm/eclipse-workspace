package com.max.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Test3 {
	public static void main(String[] args) {

		List<Student> list = new ArrayList<>();
		list.add(new Student("Tom", 18, 100, "class05"));
		list.add(new Student("Jerry", 22,70, "class04"));
		list.add(new Student("Owen", 20, 90, "class05"));
		list.add(new Student("Jim", 30, 80, "class05"));
		list.add(new Student("Steve", 28, 66, "class6"));
		list.add(new Student("Kevin", 24, 100, "class04"));

		HashMap<String,List<Student>> map = new HashMap<String, List<Student>>();

		for (Student student : list) {
			String key = student.getGrade();
			if(map.containsKey(key)) {
				map.get(key).add(student);
			}else {
				List<Student> arrlist = new ArrayList<>();
				arrlist.add(student);
				map.put(key, arrlist);
			}
		}
		
		double sum = 0;
		Integer Allage = 0;
		String grade = null;
		for (String key2 : map.keySet()) {
			  List<Student> keylist = map.get(key2);
			  for (Student keyStu : keylist) {
				  grade = keyStu.getGrade();
				  sum=sum+keyStu.getScore();
				  Allage = Allage + keyStu.getAge();;
			  }
			  System.out.println(grade+"班, 人数"+keylist.size()+"  成绩" + sum /keylist.size());
			  sum = 0;
		}
				System.out.println("平均年龄："+Allage/list.size());



	}
}
