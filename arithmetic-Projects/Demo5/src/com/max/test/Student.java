package com.max.test;

public class Student {
	
	private String name;
	private Integer age;
	private double score;
	private String grade;
	
	
	
	
	
	
	public Student() {
		super();
	}
	public Student(String name, Integer age, double score, String grade) {
		super();
		this.name = name;
		this.age = age;
		this.score = score;
		this.grade = grade;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", score=" + score + ", grade=" + grade + "]";
	}
	
	
	
	
	

}
