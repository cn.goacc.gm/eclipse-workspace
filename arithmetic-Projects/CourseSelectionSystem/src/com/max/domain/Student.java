package com.max.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter@Getter@ToString
public class Student {
	private Long sid;
	private String username;
	private String password;
	private Department department;
	private List<Course> courses;
	
}
