package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter@Getter@ToString
public class Department {
	private Long did;
	private String dname;
}
