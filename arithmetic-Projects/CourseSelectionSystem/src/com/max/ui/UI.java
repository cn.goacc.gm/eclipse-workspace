package com.max.ui;

import java.util.List;
import java.util.Scanner;

import com.max.domain.Course;
import com.max.domain.Student;
import com.max.service.CourseService;
import com.max.service.StudentService;

public class UI {
	private static Student student = null;
	private static StudentService ss = new StudentService();
	private static CourseService cs = new CourseService();
	
	
	private static void index() {
		Scanner sc = new Scanner(System.in);
		System.out.println(student.getDepartment().getDname() +"---"+student.getUsername()+"登录成功");
		List<Course> courses = cs.getAllCourDao();
		while(true) {
			System.out.println("编号\t\t课程名");
			for (Course course : courses) {
				System.out.println(" "+course.getCid() +"\t\t"+ course.getCname());
			}
			
			System.out.println("请选择课程编号：");
			int select = sc.nextInt();
			boolean is = ss.isCourseByUidAndCid(student.getSid(),select);
			if(is) {
				//选过了
				System.out.println("该门课已经选过了");
			}else {
				ss.addStudent_course_rel(student.getSid(),select);
				System.out.println("选课成功！！");
				//显示学生的所以选课信息
				Student newStu = ss.getAllStudentCourse(student.getSid());
				for (Course course : newStu.getCourses()) {
					System.out.println(course);
				}
				System.out.println("请选择是否继续选择：y or n    （输入y则重新选择，输入n则结束项目）输入任意字符继续选择");
				String sle = sc.next();
				if(sle.equalsIgnoreCase("n")) {
					System.exit(0);
				}
				
				if(sle.equalsIgnoreCase("y")) {
					// 删除所有已选课程
					ss.deleteStudent_course_relByUid(student.getSid());
					System.out.println("选择的课程已经被删除 请重新选择s");
					
				}
				
			}
			
		}
		
		
	}
	
	
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("欢迎进入讯飞学院选课系统");
			System.out.println("请登录：");
			System.out.println("请输入用户名：");
			String username = sc.next();
			System.out.println("请输入密码：");
			String password = sc.next();
			student = ss.getStudentByUsernameAndPassword(username,password);
			if(student != null) {
				index();
				break;
			}
			System.out.println("账号或密码错误，请重新登录！！");
			
		}
		
	}

}
