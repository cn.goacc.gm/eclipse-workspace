package com.max.dao;

import java.sql.ResultSet;
import java.util.List;

import com.max.domain.Course;
import com.max.handler.BeanListHandler;
import com.max.utlis.DBUtil;

public class CourseDao {

	private DBUtil db = new DBUtil(); 
	
	public List<Course> SelectAllCourse() throws Exception {
		ResultSet resultSet = db.query("select * from course");
		List<Course> list = new BeanListHandler<Course>(Course.class).handler(resultSet);
		return list;
	}

}
