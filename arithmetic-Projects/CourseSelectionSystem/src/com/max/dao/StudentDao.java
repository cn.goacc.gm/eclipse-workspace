package com.max.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.max.domain.Course;
import com.max.domain.Department;
import com.max.domain.Student;
import com.max.handler.IResultSethandler;
import com.max.utlis.DBUtil;

public class StudentDao {

	private DBUtil db = new DBUtil(); 
	
	public Student SelectStudentByUsernameAndPassword(String username, String password) throws Exception {
		String sql = "select * from student as s left join department as d on s.did = d.did where username = ? and password = ?";
		ResultSet resultSet = db.query(sql, username,password);
		Student student = new MyHandleResultSet().handler(resultSet);
		return student;
	}
	
	
	
	class MyHandleResultSet implements IResultSethandler<Student>{

		@Override
		public Student handler(ResultSet rs) throws Exception {
			Student student = null;
			Department department = new Department();
			if(rs.next()) {
				student = new Student();
				student.setSid(rs.getLong("sid"));
				student.setUsername(rs.getString("username"));
				student.setPassword(rs.getString("password"));
				department.setDname(rs.getString("dname"));
				student.setDepartment(department);
				
			}
			return student;
		}
		
	}



	public boolean SelectStudent_course_relByUidAndCid(Long sid, int select) throws SQLException {
		
		ResultSet resultSet = db.query("select * from student_course_rel where sid = ? and cid = ? ", sid,select);
		return resultSet.next();
		
		
	}



	public void insertStudent_course_rel(Long sid, int select) {
		db.execute("insert into student_course_rel values(?,?) ", sid,select);
	}



	public Student SelectStudentAllCourse(Long sid) throws Exception {
		
		ResultSet resultSet = db.query("  select * from course where cid in(select cid from student_course_rel where sid = ?) ", sid);
		Student student = new MyHandleListCourseResultSet().handler(resultSet);
		return student;
	}
	
	class MyHandleListCourseResultSet implements IResultSethandler<Student>{

		@Override
		public Student handler(ResultSet rs) throws Exception {
			Student student = new Student();
			ArrayList<Course> courses = new ArrayList<>();
			while(rs.next()) {
				Course course = new Course();
				course.setCid(rs.getInt("cid"));
				course.setCname(rs.getString("cname"));
				courses.add(course);
			}
			student.setCourses(courses);
			return student;
		}
		
	}



	public void deleteStudent_course_relByUid(Long sid) {
		db.execute("delete from  student_course_rel where sid = ?", sid);
	}
	
	

}
