package com.max.test;

import java.util.List;

import org.junit.Test;
import org.w3c.dom.CDATASection;

import com.max.dao.CourseDao;
import com.max.dao.StudentDao;
import com.max.domain.Course;
import com.max.domain.Student;
import com.max.service.StudentService;

public class Test2 {

	private StudentService ss = new StudentService();
	private CourseDao cd = new CourseDao();
	
	@Test
	public void test2() {
		Student student = ss.getStudentByUsernameAndPassword("max", "123");
		System.out.println(student);
		
	}
	@Test
	public void test3() throws Exception {
		List<Course> selectAllCourse = cd.SelectAllCourse();
		System.out.println(selectAllCourse);
		
	}
	@Test
	public void test4() throws Exception {
		
		Student student = ss.getAllStudentCourse(1L);
		System.out.println(student.getCourses());
		
	}
	
}
