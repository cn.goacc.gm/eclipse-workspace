package com.max.utlis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DBUtil {
	
	private static final String DRIVER_CLASS = Util.getDBValue("driver");
	private static final String URL = Util.getDBValue("url");
	private static final String USER = Util.getDBValue("user");
	private static final String PASSWORD = Util.getDBValue("password");
	
	static {
		// 加载驱动
		try {
			Class.forName(DRIVER_CLASS);
		} catch (Exception e) {
			System.out.println("加载数据库驱动出错");
		}
	}
	
	/**
	 * 获得数据库连接
	 * @return
	 */
	private Connection getConnection() {
		try {
			return DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (Exception e) {
			System.out.println("获得数据库连接出错");
		}
		return null;
	}
	
	
	
	/**
	 * select方法
	 * @param sql
	 * @param params
	 * @return resultset
	 */
	public ResultSet query(String sql , Object ... params) {
		
		Connection con = this.getConnection();
		ResultSet rs  = null;
		if(con != null) {
			// 获得数据库执行对象
			try {
				PreparedStatement ps = con.prepareStatement(sql);
				
				if(params != null) {
					for (int i = 0; i < params.length; i++) {
						// 根据目标类型自动设置类型
						ps.setObject(i+1, params[i]);
					}
				}
				rs = ps.executeQuery();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return rs;
	}
	
	
	
	/**
	 * 针对insert update delete 的方法
	 * @param sql
	 * @param params
	 */
	public void execute(String sql , Object ... params) {
		Connection con = this.getConnection();
		PreparedStatement ps = null;
		if(con != null) {
			// 获得数据库执行对象
			try {
				ps = con.prepareStatement(sql);
				
				if(params != null) {
					for (int i = 0; i < params.length; i++) {
						// 根据目标类型自动设置类型
						ps.setObject(i+1, params[i]);
					}
				}
				ps.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if(ps != null) {
					try {
						ps.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
				if(con != null) {
					try {
						con.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
				
			}
		}
		
		
	}
	
	
	
	
	
	
	
	
	
}
