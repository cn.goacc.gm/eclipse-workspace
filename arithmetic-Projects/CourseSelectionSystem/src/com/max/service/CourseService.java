package com.max.service;

import java.util.List;

import com.max.dao.CourseDao;
import com.max.domain.Course;

public class CourseService {

	private CourseDao cd = new CourseDao();
	public List<Course> getAllCourDao() {
		
		List<Course> selectAllCourse = null;
		try {
			selectAllCourse = cd.SelectAllCourse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return selectAllCourse;
	}

}
