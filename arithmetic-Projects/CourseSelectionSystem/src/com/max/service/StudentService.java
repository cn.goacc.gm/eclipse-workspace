package com.max.service;

import java.sql.SQLException;

import com.max.dao.StudentDao;
import com.max.domain.Student;

public class StudentService {
	private StudentDao sd = new StudentDao();

	public Student getStudentByUsernameAndPassword(String username, String password) {
		
		Student student = null;
		try {
			student = sd.SelectStudentByUsernameAndPassword(username,password);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return student;
	}

	public boolean isCourseByUidAndCid(Long sid, int select) {
		
		try {
			return sd.SelectStudent_course_relByUidAndCid(sid,select);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public void addStudent_course_rel(Long sid, int select) {
		
		sd.insertStudent_course_rel(sid,select);
		
	}

	public Student getAllStudentCourse(Long sid) {
		Student selectStudentAllCourse = null;
		try {
			selectStudentAllCourse = sd.SelectStudentAllCourse(sid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return selectStudentAllCourse;
	}

	public void deleteStudent_course_relByUid(Long sid) {
		
		sd.deleteStudent_course_relByUid(sid);
		
	}

}
