package domain;

public class Student {
	private String name;
	private Integer age;
	private String sex;
	
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof Student) {
			Student n = (Student) obj;
			return n.getName().equals(name);
			
		}
		
		return false;
	}
	
	
	
	
	public Student() {
		super();
	}
	public Student(String name, Integer age, String sex) {
		super();
		this.name = name;
		this.age = age;
		this.sex = sex;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", sex=" + sex + "]";
	}
	
	
	
	
	

}
