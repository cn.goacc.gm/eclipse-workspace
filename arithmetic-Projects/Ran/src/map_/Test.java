package map_;

import java.util.HashMap;
import java.util.Map;

public class Test {

	public static void main(String[] args) {
		
		Integer[] a= {1,2,3,4,5,1,2,3,4,5,5};
		
		Map<Integer, Integer> map = new HashMap<>();
		
		
		for (Integer integer : a) {
			
			if(map.containsKey(integer)){
				Integer value = map.get(integer);
				map.put(integer,value+=1);
			}else {
				map.put(integer, 1);
			}
			
			
		}
		
		System.out.println(map);
		

	}

}
