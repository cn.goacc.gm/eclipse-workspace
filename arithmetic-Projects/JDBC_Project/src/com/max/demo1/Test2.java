package com.max.demo1;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

public class Test2 {
	
	public static void main(String[] args) throws Exception {
		
			Class<?> class1 = Class.forName("com.max.demo1.Account");
			Object newInstance = class1.newInstance();
			
			BeanInfo beanInfo = Introspector.getBeanInfo(class1,Object.class);
			PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
			
			for (PropertyDescriptor pd : pds) {
//				System.out.println("获取字段名  "+pd.getName());
//				System.out.println("获取字段类型  "+pd.getPropertyType());
//				System.out.println("获取get方法  "+pd.getReadMethod());
//				System.out.println("获取set方法  "+pd.getWriteMethod());
				
				System.out.println(pd.getReadMethod());
				System.out.println(pd.getReadMethod().invoke(newInstance));
				
				System.out.println(" ------------ ");
				
			}
			
			
		
		
	}

}
