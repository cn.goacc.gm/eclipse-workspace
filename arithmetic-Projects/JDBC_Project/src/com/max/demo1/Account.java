package com.max.demo1;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter@Setter@ToString

public class Account {
	private Integer id;
	private String name;
	private Double money;
	
}
