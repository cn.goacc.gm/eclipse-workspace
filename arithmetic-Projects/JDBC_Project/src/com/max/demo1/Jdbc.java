package com.max.demo1;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Jdbc {
	public static void main(String[] args) {
		try {
			//加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			//获得对象工场
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spring?characterEncoding=utf8", "root", "li767787498");
			
			//连接对象
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from account");
			
			//自动封装结果集
			
			//通过字节码创建对象
			
			Class class1 = Class.forName("com.max.demo1.Account");
			Object obj = class1.newInstance();
			
			//拿到属性
			BeanInfo info = Introspector.getBeanInfo(obj.getClass(), Object.class);
			
			while (resultSet.next()) {
				
				//通过内省拿到get set方法
				PropertyDescriptor[] pds = info.getPropertyDescriptors();
				
				//拿到属性名 和 属性类型
//				String name = pd.getName();
//				Class<?> type = pd.getPropertyType();
				for (PropertyDescriptor pd : pds) {
					//获取数据的值
					Object object = resultSet.getObject(pd.getName());
					// 封装该对象
					pd.getWriteMethod().invoke(obj,object);
				}
				
			}
			
			System.out.println((Account)obj);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}

}
