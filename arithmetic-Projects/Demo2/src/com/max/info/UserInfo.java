package com.max.info;

public class UserInfo {
	private String username;
	private String computer;
	private double price;
	
	
	public UserInfo(String username, String computer, double price) {
		super();
		this.username = username;
		this.computer = computer;
		this.price = price;
	}
	

	public UserInfo() {
		super();
	}


	@Override
	public String toString() {
		return "UserInfo [username=" + username + ", computer=" + computer + ", price=" + price + "]";
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getComputer() {
		return computer;
	}
	public void setComputer(String computer) {
		this.computer = computer;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
