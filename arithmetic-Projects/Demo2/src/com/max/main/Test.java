package com.max.main;

import java.awt.List;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.concurrent.SynchronousQueue;

public class Test {
	public static void main(String[] args) throws Exception {
		Student student = new Student();
		student.setName("名字1");
		student.setAge(24);
		student.setSex("男");
//		System.out.println(student);
		
		
		//通过字节码创建javaBead对象
		Class forName = Class.forName("com.max.main.Student");
		Object newStu = forName.newInstance();
		
		//内省
		/* 获取User-BeanInfo对象
        *      1、Introspector类
        *              是一个工具类，提供了一系列取得BeanInfo的方法；
        *      2、BeanInfo接口
        *              对一个JavaBean的描述，可以通过它取得Bean内部的信息；
        *      3、PropertyDescriptor属性描述器类
        *              对一个Bean属性的描述，它提供了一系列对Bean属性进行操作的方法
        */
		
		BeanInfo beanInfo = Introspector.getBeanInfo(forName);
		
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
//		for (PropertyDescriptor pds : propertyDescriptors) {
//			Method readMethod = pds.getReadMethod();   //查看get方法 
//			Method writeMethod = pds.getWriteMethod(); //查看set方法
//			String name = pds.getName();	 			//查看字段
//		}
		
		for (PropertyDescriptor pds : propertyDescriptors) {
			
			Method writeMethod = pds.getWriteMethod(); //查看set方法
			Method readMethod = pds.getReadMethod();   //查看get方法 
			
			Object in = readMethod.invoke(student);
			
			
//			writeMethod.invoke(newStu, );
			
		}
		
		
	
//		System.out.println(newStu);
		
		
		
		
	
		 
		
	
		
		
		/* BeanInfo userBeanInfo = Introspector.getBeanInfo(User.class) ;
	        PropertyDescriptor[] pds = userBeanInfo.getPropertyDescriptors() ;
	        for (PropertyDescriptor pd : pds) {
	            Method method = pd.getReadMethod() ;
	            String methodName = method.getName() ;
	            Object result = method.invoke(user) ;
	            System.out.println(methodName + "：" + result);
	        }*/
		
		
		/*//获取name属性的属性描述器
        PropertyDescriptor pd = new PropertyDescriptor("age", user.getClass()) ;
        //得到name属性的getter方法
        Method readMethod = pd.getReadMethod() ;
        //执行getter方法，获取返回值，即age属性的值
        Integer result = (Integer) readMethod.invoke(user) ;
        System.out.println("修改前小明的年龄" + "：" + result);
        //得到name属性的setter方法
        Method writeMethod = pd.getWriteMethod() ;
        //执行setter方法，修改age属性的值
        writeMethod.invoke(user, 38) ;
        System.out.println("修改后小明的年龄" + "：" + user.getAge());*/
		
		
		
	}

}
