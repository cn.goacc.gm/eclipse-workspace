package com.max.demo4;

public class Manager implements Employee {
//	经理:姓名、编号、固定工资、当月薪水，方法有计算月薪的方法
	private String name;
	private String id;
	private double GDsalary;
	private double month_money;
	
	
	
	public Manager(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}

	@Override
	public void Cpmonth_money() {
		this.GDsalary = 8000;
		this.month_money = GDsalary;
		
	}
	

	@Override
	public String toString() {
		return "Manager(经理) [name=" + name + ", id=" + id + ", GDsalary=" + GDsalary + ", month_money=" + month_money + "]";
	}


	


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public double getGDsalary() {
		return GDsalary;
	}


	public void setGDsalary(double gDsalary) {
		GDsalary = gDsalary;
	}


	public double getMonth_money() {
		return month_money;
	}


	public void setMonth_money(double month_money) {
		this.month_money = month_money;
	}
	

}
