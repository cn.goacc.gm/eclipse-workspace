package com.max.demo4;

// 兼职技术人员：姓名、编号、工作小时、每小时工资额、当月薪水、方法有计算月薪的方法；
public class Technician implements Employee {
	private String name;
	private String id;
	private Integer hour;
	private double hour_money;
	private double month_money;
	
	public Technician(String name, String id, Integer hour) {
		super();
		this.name = name;
		this.id = id;
		this.hour = hour;
	}

	
	public Technician() {
		super();
	}


	@Override
	public void Cpmonth_money() {
//		兼职技术人员按100元/小时领取月薪
		this.hour_money = 100;
		this.month_money = hour * hour_money;
	}


	@Override
	public String toString() {
		return "Technician(兼职技术人员) [name=" + name + ", id=" + id + ", hour=" + hour + ", hour_money=" + hour_money
				+ ", month_money=" + month_money + "]";
	}




	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Integer getHour() {
		return hour;
	}


	public void setHour(Integer hour) {
		this.hour = hour;
	}


	public double getHour_money() {
		return hour_money;
	}


	public void setHour_money(double hour_money) {
		this.hour_money = hour_money;
	}


	public double getMonth_money() {
		return month_money;
	}


	public void setMonth_money(double month_money) {
		this.month_money = month_money;
	}
	
	
	
	
	
	


}
