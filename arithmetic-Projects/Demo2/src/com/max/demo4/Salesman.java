package com.max.demo4;

// 兼职销售员：姓名、编号、销售额、销售额提成、当月薪水、方法有计算月薪的方法
public class Salesman implements Employee{
	private String name;
	private String id;
	private double sale;
	private double sale_tc;
	private double month_money;
	

	public Salesman(String name, String id, double sale) {
		super();
		this.name = name;
		this.id = id;
		this.sale = sale;
	}


	public Salesman() {
		super();
	}
	
	@Override
	public void Cpmonth_money() {
//		按当月销售额的4%提成
		sale_tc = sale * 0.04;
		month_money = sale_tc;
	}



	@Override
	public String toString() {
		return "Salesman(兼职销售员) [name=" + name + ", id=" + id + ", sale=" + sale + ", sale_tc=" + sale_tc + ", month_money="
				+ month_money + "]";
	}





	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public double getSale() {
		return sale;
	}


	public void setSale(double sale) {
		this.sale = sale;
	}


	public double getSale_tc() {
		return sale_tc;
	}


	public void setSale_tc(double sale_tc) {
		this.sale_tc = sale_tc;
	}


	public double getMonth_money() {
		return month_money;
	}


	public void setMonth_money(double month_money) {
		this.month_money = month_money;
	}
	
	
	
	

}
