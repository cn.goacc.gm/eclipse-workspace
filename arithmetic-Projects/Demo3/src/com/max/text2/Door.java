package com.max.text2;

public abstract class Door {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Door [type=" + type + "]";
	}

	
	
	
}
