package com.max.text2;

//铁制门
public class IronDoor extends Door implements DoorFunction{

	@Override
	public void password() {
		this.setType("铁质密码门");
	}
	
	@Override
	public void security() {
		this.setType("铁质防盗");
	}
	
}
