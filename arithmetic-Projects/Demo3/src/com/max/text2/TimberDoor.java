package com.max.text2;

//木质门
public class TimberDoor extends Door implements DoorFunction{

	@Override
	public void password() {
		this.setType("木质密码");
	}

	@Override
	public void security() {
		this.setType("木质防盗");
	}
	

}
