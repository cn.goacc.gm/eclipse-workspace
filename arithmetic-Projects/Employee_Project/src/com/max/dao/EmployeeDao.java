package com.max.dao;

import java.util.ArrayList;
import java.util.List;

import com.max.domain.Employee;
import com.max.util.FileUtil;

public class EmployeeDao {
	
	
	public List<Employee> getAllData() {
		List<Employee> list = (List<Employee>) FileUtil.readObject();
		return list;
	}
	
	public void setAllDate(List<Employee> list) {
		FileUtil.clearFile();
		FileUtil.writeObject(list);
	}
	

}
