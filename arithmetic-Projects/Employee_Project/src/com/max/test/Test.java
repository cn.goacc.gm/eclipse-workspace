package com.max.test;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.max.domain.Employee;
import com.max.util.FileUtil;

public class Test {
	public static void main(String[] args) throws Exception {
		
		List<Employee> list = new ArrayList<>();
		
		list.add(new Employee("张三", "abc", "123", 22, 5000, "安徽蚌埠"));
		list.add(new Employee("李四", "qw", "123", 25, 7000, "安徽合肥"));
		list.add(new Employee("王五", "cz", "123", 30, 9000, "安徽芜湖"));
		list.add(new Employee("刘三", "ly", "123", 33, 12000, "安徽淮南"));
		
//		FileUtil.writeObject(list);
		List<Employee> list2 = (List<Employee>) FileUtil.readObject();
		System.out.println(list2);
		
		
		
	}
	public static <T> void tets(T property,Employee em ) throws Exception {
		 
		PropertyDescriptor pd = new PropertyDescriptor("Name", em.getClass());
		Method method = pd.getWriteMethod();
		method.invoke(em, property);
		System.out.println(em);
	}
	
	
}
