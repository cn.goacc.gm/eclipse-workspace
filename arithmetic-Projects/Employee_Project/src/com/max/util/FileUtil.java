package com.max.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileUtil {
	
	private static final String EMPLOYEE_FILE = getProPath()+"/EmployeeFile.txt";
	
	
	//获取项目路径
	public static String getProPath() {
		File file = new File("");
		try {
			return file.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	/**
	 * 清空文件内容
	 */
	public static void clearFile() {
		try {
			File file = new File(EMPLOYEE_FILE);
			file.delete();
			
			File file2 = new File(EMPLOYEE_FILE);
			file2.createNewFile();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 反序列化文件到对象
	 * @return
	 */
	public static Object readObject() {
		ObjectInputStream ois = null;
		Object result = null;
		try {
			// 输入流
			ois = new ObjectInputStream(new FileInputStream(EMPLOYEE_FILE));
			result = ois.readObject();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(ois != null) {
				try {
					ois.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * 序列化对象到文件里
	 * @param obj
	 */
	public static void writeObject(Object obj) {
		
		ObjectOutputStream oos = null;
		
		try {
			File file = new File(EMPLOYEE_FILE);
			if(!file.exists()) {
				file.createNewFile();
			}else {
				clearFile();
			}
			
			// 输出流
			oos = new ObjectOutputStream(new FileOutputStream(file));
			oos.writeObject(obj);
			oos.flush();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(oos != null) {
				try {
					oos.close();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		}
		
	}
	
	
	
	
}
