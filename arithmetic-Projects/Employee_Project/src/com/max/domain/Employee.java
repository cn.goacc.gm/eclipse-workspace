package com.max.domain;

import java.io.Serializable;

public class Employee implements Serializable{
	
	private String name;
	
	private String loginName;
	
	private String password;
	
	private Integer age;
	
	private double salary;
	
	private String address;

	public Employee() {
	}

	
	
	
	
	public Employee(String name, String loginName, String password, Integer age, double salary, String address) {
		super();
		this.name = name;
		this.loginName = loginName;
		this.password = password;
		this.age = age;
		this.salary = salary;
		this.address = address;
	}





	@Override
	public String toString() {
		return "Employee [name=" + name + ", loginName=" + loginName + ", password=" + password + ", age=" + age
				+ ", salary=" + salary + ", address=" + address + "]";
	}





	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	
	
	
	
	

}
