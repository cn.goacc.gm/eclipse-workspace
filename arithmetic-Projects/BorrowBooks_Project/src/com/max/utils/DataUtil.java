package com.max.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DataUtil {
	
	public static String stampToDate(Long s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        long lt = new Long(""+s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
	
	
	
	public static String dateToStamp(String s) throws ParseException{
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        res = String.valueOf(ts);
        return res;
    }
	
	
	public static Long getDistanceTime(long time1, long time2) {
	    long day = 0;
	    long hour = 0;
	    long diff;

	    if (time1 < time2) {
	            diff = time2 - time1;
	        } else {
	            diff = time1 - time2;
	        }
	        day = diff / (24 * 60 * 60 * 1000);
	        if (day != 0) return day;
	        
	        return 0L;
	}
	

}
