package com.max.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.max.domian.Book;
import com.max.domian.User;
import com.max.service.BookService;
import com.max.service.UserService;
import com.max.utils.DataUtil;

public class HomeUI {

	/*
	 *账号：abc 
	 *密码：123
	 * */
	
	
	private static UserService userService = new UserService();
	private static BookService bookService = new BookService();
	
	private static String Overall_User_Name = null;

	private static void login() {
		boolean flag = true;
		String name = null;
		while(flag) {
			Scanner sc = new Scanner(System.in);
			System.out.println("欢迎进入蚌埠电子学院借书系统（文件版）");
			System.out.println("请登录");
			System.out.println("请输入用户名：");
			name = sc.next();
			System.out.println("请输入密码：");
			String passwore = sc.next();
			flag= userService.query(name,passwore);
			if(flag) {
				System.out.println("账号或密码不正确，请重新登录：");
			}
		}
		if(!flag && name != null) {
			Overall_User_Name = name;
			Home();
		}
		
	}



	private static void Home() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("登录成功");
		while(true) {
			System.out.println("请选择功能:1：借书\t 2：查看个人借书记录\t 3：退出");
			Integer select = sc.nextInt();
			switch (select) {
			case 1:
				borrow_books();
				break;
			case 2:
				check_record();
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("选择错误，请重新选择");
				break;
			}
		}
		
	}

	private static void PrintfBook() {
		System.out.println("借书");
		List<Book> list = bookService.getAllBook();
		System.out.println("编号\t名称\t类型\t可借周期");
		for (Book book : list) {
			System.out.println(book.getId() +"\t"+ book.getBook_Name() +"\t"+ book.getBook_type() +"\t"+ book.getPeriod()/7 +"周");
		}
	}
	
	
	public static void borrow_books() {
		Scanner sc = new Scanner(System.in);
		List<Book> BookIdlist = new ArrayList<>();
		Map<String, List<Book>> map = new HashMap<>();
		
		PrintfBook();
		
		List<Book> list = bookService.getAllBook();
		boolean flag = true;
		while(flag) {
			System.out.println("请输入编号：");
			Integer sc_BookId = sc.nextInt();
			boolean isbook = false;
			for (Book book : list) {
				if(sc_BookId.intValue() == book.getId().intValue()) {
					// 设置时间
					book.setTime(System.currentTimeMillis());
					BookIdlist.add(book);
					System.out.println("借书成功！");
					isbook = true;
					break;
				}
			}
			if(!isbook) {
				System.out.println("借书失败！");
			}
			
			System.out.println("按回车键继续添加借书  （输入ok 借书完成）");
			String is = sc.next();
			if(is.equals("ok") || is.equals("OK")) {
				flag = false;
			} else {
				PrintfBook();
			}
		}
		map.put(Overall_User_Name, BookIdlist);
		userService.setUserMap(Overall_User_Name,map);
		System.out.println("已存入数据库中。。。");
		
	}
	
	
	public static void check_record() {
		System.out.println("查看个人借书记录：");
		User user = userService.getOneUser(Overall_User_Name);
		if(user != null) {
			System.out.println("编号\t名称\t借书日期\t是否逾期");
			Map<String, List<Book>> userBookMap = user.getUserBookMap();
			for (String key : userBookMap.keySet()) {
				long currentTimeMillis = System.currentTimeMillis();
				List<Book> list = userBookMap.get(key);
				for (Book book : list) {
					String time = DataUtil.getDistanceTime(currentTimeMillis, book.getTime()) <= book.getPeriod().intValue() ? "未逾期" : "逾期";
					System.out.println(book.getId() +"\t"+ book.getBook_Name() +"\t"+ DataUtil.stampToDate(book.getTime()) +"\t"+ time);
//					System.out.println(" 借书日期 与 当前日期相差天数： "+ DataUtil.getDistanceTime(currentTimeMillis, book.getTime()));
				}
				
			}
			
			
		}
		
	}



	public static void main(String[] args) {

		login();

	}

}
