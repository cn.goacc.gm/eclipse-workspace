package com.max.dao;

import java.io.File;
import java.util.List;

import com.max.domian.User;
import com.max.utils.FileName;
import com.max.utils.FileUtil;

public class UserDao {

	FileUtil fileUtil =  new FileUtil(FileName.USER_FILE);
	
	public List<User> getUserData(){
		return (List<User>) fileUtil.readObject();
		
	}
	
	public void setUserData(List<User> list ) {
		fileUtil.clearFile();
		fileUtil.writeObject(list);
		
	}
	
	
	
	
}
