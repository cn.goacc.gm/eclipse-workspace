package com.max.dao;

import java.util.List;

import com.max.domian.Book;
import com.max.domian.User;
import com.max.utils.FileName;
import com.max.utils.FileUtil;

public class BookDao {
	
	FileUtil fileUtil =  new FileUtil(FileName.BOOKS_FILE);
	
	public List<Book> getBookData(){
		return (List<Book>) fileUtil.readObject();
		
	}
	
	public void setBookData(List<Book> list ) {
		fileUtil.clearFile();
		fileUtil.writeObject(list);
		
	}
	
	


}
