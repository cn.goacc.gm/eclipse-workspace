package com.max.domian;

import java.io.Serializable;

public class Book implements Serializable{
	
	private static final long serialVersionUID = -5049163187382079265L;
	
	private Integer id;
	private String Book_Name;
	private String Book_type;
	private Integer Period;
	private Long Time;
	
	
	public Book() {
		super();
	}

	
	public Book(Integer id, String book_Name, String book_type, Integer period) {
		super();
		this.id = id;
		Book_Name = book_Name;
		Book_type = book_type;
		Period = period;
	}


	@Override
	public String toString() {
		return "Book [id=" + id + ", Book_Name=" + Book_Name + ", Book_type=" + Book_type + ", Period=" + Period + "]";
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBook_Name() {
		return Book_Name;
	}
	public void setBook_Name(String book_Name) {
		Book_Name = book_Name;
	}
	public String getBook_type() {
		return Book_type;
	}
	public void setBook_type(String book_type) {
		Book_type = book_type;
	}
	public Integer getPeriod() {
		return Period;
	}
	public void setPeriod(Integer period) {
		Period = period;
	}


	public Long getTime() {
		return Time;
	}


	public void setTime(Long time) {
		Time = time;
	}

	
	
	

}
