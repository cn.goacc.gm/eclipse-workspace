package com.max.domian;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.jws.soap.SOAPBinding.Use;

public class User implements Serializable{
	
	
	private static final long serialVersionUID = -6736985629815009366L;
	
	private String User_Name;
	private String Password;
	private Map<String, List<Book>> UserBookMap;
	
	
	public User() {
		super();
	}


	public User(String user_Name, String password) {
		super();
		User_Name = user_Name;
		Password = password;
	}



	@Override
	public boolean equals(Object obj) {
		if(obj instanceof User) {
			User user = (User)obj;
			return user.getUser_Name().equals(User_Name) && user.getPassword().equals(Password);
		}
		
		return false;
	}


	@Override
	public String toString() {
		return "User [User_Name=" + User_Name + ", Password=" + Password+"]";
	}
	
	


	public String getUser_Name() {
		return User_Name;
	}




	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}




	public String getPassword() {
		return Password;
	}




	public void setPassword(String password) {
		Password = password;
	}


	public Map<String, List<Book>> getUserBookMap() {
		return UserBookMap;
	}


	public void setUserBookMap(Map<String, List<Book>> userBookMap) {
		UserBookMap = userBookMap;
	}


	

}
