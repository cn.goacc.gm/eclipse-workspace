package com.max.test;

import java.util.ArrayList;
import java.util.List;

import com.max.dao.BookDao;
import com.max.domian.Book;

public class BookTest {
	
	public static void main(String[] args) {
		
		
		List<Book> list = new ArrayList<>();
		list.add(new Book(1, "java", "开发", 14));
		list.add(new Book(2, "C", "开发", 14));
		list.add(new Book(3, "计算机基础", "基础课", 14));
		
		BookDao bookDao = new BookDao();
//		bookDao.setBookData(list);
		
		List<Book> bookData = bookDao.getBookData();
		for (Book book : bookData) {
			System.out.println(book);
		}
		
		
	}

}
