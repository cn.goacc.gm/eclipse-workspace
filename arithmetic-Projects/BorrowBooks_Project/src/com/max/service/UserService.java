package com.max.service;

import java.util.List;
import java.util.Map;

import com.max.dao.UserDao;
import com.max.domian.Book;
import com.max.domian.User;

public class UserService {
	
	UserDao userDao = new UserDao();
	private List<User> list = userDao.getUserData();

	public boolean query(String name, String passwore) {
		for (User user : list) {
			return !user.equals(new User(name, passwore));
		}
		return true;
	}

	public void setUserMap(String overall_User_Name, Map<String, List<Book>> map) {
		
		for (User user : list) {
			if(user.getUser_Name().equals(overall_User_Name)) {
				for (String key : map.keySet()) {
					List<Book> list2 = map.get(key);
					for (Book book : list2) {
						user.getUserBookMap().get(overall_User_Name).add(book);
					}
					
				}
			}
		}
		
		
		userDao.setUserData(list);
		
		
	}

	public User getOneUser(String overall_User_Name) {
		for (User user : list) {
			if(user.getUser_Name().equals(overall_User_Name)) {
				return user;
			}
		}
		
		return null;
	}

}
