package com.max.web;

import java.util.List;
import java.util.Scanner;

import com.max.domain.PageBean;
import com.max.domain.Student;
import com.max.service.StudentService;

public class WebAction {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StudentService stuService = new StudentService();
		
		//设置当前页
		int i = 1;
		//设置一页展示多少条数据
		int pagesize = 5;
		
		while(true) {
			sc.nextLine();
			PageBean<Student> pageStu = stuService.getPageStu(i, pagesize);
			System.out.println(pageStu);
			i++;
			
		}

	}

}
