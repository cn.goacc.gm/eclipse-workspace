package com.max.dao;

import java.util.ArrayList;
import java.util.List;

import com.max.domain.Student;

public class Dao {
	private static List<Student> list =new ArrayList<>();
	
	static {
		
		Student student = new Student("张三","男",23);
		Student student2 = new Student("李四","男",18);
		Student student3 = new Student("王五","男",23);
		Student student4 = new Student("赵六","女",21);
		Student student5 = new Student("test","男",23);
		Student student6 = new Student("王五2","男",23);
		Student student7 = new Student("赵六3","女",21);
		Student student8 = new Student("test4","男",23);
		Student student9 = new Student("王五5","男",23);
		Student student10 = new Student("赵六6","女",21);
		Student student11 = new Student("test7","男",23);
		list.add(student);
		list.add(student2);
		list.add(student3);
		list.add(student4);
		list.add(student5);
		list.add(student6);
		list.add(student7);
		list.add(student8);
		list.add(student9);
		list.add(student10);
		list.add(student11);
	}
	public List<Student> getListStu(){
		
		return list;
		
	}
	public Integer getDataCount() {
		
		return list.size();
	}
	public List<Student> getPageList(Integer index, Integer pageSize,Integer TotalPage) {
		List<Student> pagelist = new ArrayList<>();
		pageSize = index + pageSize;
		System.out.println( index +"  "+pageSize);
			for(int i = index;i<=pageSize;i++) {
				pagelist.add(list.get(index));
			}
		
		return pagelist;
	}
	

}
