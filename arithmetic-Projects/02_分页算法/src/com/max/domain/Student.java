package com.max.domain;

public class Student {
	private String StuName;
	private String StuSex;
	private Integer age;
	
	

	public Student(String stuName, String stuSex, Integer age) {
		super();
		StuName = stuName;
		StuSex = stuSex;
		this.age = age;
	}

	public Student() {
		super();
	}


	@Override
	public String toString() {
		return "Student [StuName=" + StuName + ", StuSex=" + StuSex + ", age=" + age + "]";
	}
	
	
	
	public String getStuName() {
		return StuName;
	}
	public void setStuName(String stuName) {
		StuName = stuName;
	}
	public String getStuSex() {
		return StuSex;
	}
	public void setStuSex(String stuSex) {
		StuSex = stuSex;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	
}
