package com.max.service;

import java.util.List;

import com.max.dao.Dao;
import com.max.domain.PageBean;
import com.max.domain.Student;

public class StudentService {
	private Dao dao = new Dao();
	private List<Student> list = dao.getListStu(); 
	
	
	
	public PageBean<Student> getPageStu(Integer currentPage,Integer pageSize){
		//组装Pagebean
		PageBean<Student> pageBean = new PageBean<>();
		pageBean.setCurrentPage(currentPage);
		pageBean.setPageSize(pageSize);
		//从数据库查询 总共有多少条数据
		Integer totalCount = dao.getDataCount();
		pageBean.setTotalCount(totalCount);
		//设置总页数
		pageBean.setTotalPage(pageBean.getTotalPage());
		//从数据库 查询 单页数据 (数据库查询起始下标  查询多少条数据)
		List<Student> pageStuList = dao.getPageList(pageBean.getIndex(),pageBean.getPageSize(),pageBean.getTotalPage());
		pageBean.setList(pageStuList);
		
		return pageBean;
		
	}
	

}
