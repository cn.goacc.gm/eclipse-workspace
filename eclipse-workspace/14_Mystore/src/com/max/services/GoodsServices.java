package com.max.services;

import java.util.List;

import com.max.dao.GoodsDao;
import com.max.domain.Goods;

public class GoodsServices {

	public List<Goods> findAllGoods() {
		GoodsDao goodsDao=new GoodsDao();
 		return goodsDao.finAllGoods();
	}

}
