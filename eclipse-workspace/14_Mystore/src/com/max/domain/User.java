package com.max.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter@Getter@ToString
public class User {
	private String uid;
	private String username;
	private String password;
	private String phone;
	
}
