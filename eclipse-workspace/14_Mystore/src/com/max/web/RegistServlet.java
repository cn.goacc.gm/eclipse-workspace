package com.max.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.QueryRunner;

import com.max.domain.User;
import com.max.jdbcutil.JDBCUtil;

/**
 * Servlet implementation class RegistServlet
 */
@WebServlet("/RegistServlet")
public class RegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//设置编码
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		ServletContext context = this.getServletContext();
		String code = request.getParameter("code");
		if(code.equals(context.getAttribute("checkCode"))) {
			response.getWriter().write("注册成功");
			
			Map<String, String[]> map = request.getParameterMap();
			try {
				User u=new User();
				BeanUtils.populate(u, map);
				//获取UUID
				u.setUid(UUID.randomUUID().toString());
				//写入到数据库
				QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
				String sql="insert into user values(?,?,?,?)"; 
				qr.update(sql,u.getUid(),u.getUsername(),u.getPassword(),u.getPhone());
				//跳转到登录界面
				response.setHeader("refresh", "3;url=/14_Mystore/login.jsp"); 
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else {
			response.getWriter().write("验证码错误");
			response.setHeader("refresh", "3;url=/14_Mystore/regist.jsp"); 
		}
		
	
	}

}
