package com.max.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.max.domain.Goods;
import com.max.jdbcutil.JDBCUtil;
import com.max.services.GoodsServices;

@WebServlet("/GoodsServlet")
public class GoodsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		GoodsServices goodsServices = new GoodsServices();
		List<Goods> AllGoods = goodsServices.findAllGoods();
		
		//把商品信息存放到request域
		request.setAttribute("AllGoods", AllGoods);
		//请求转发到到goods页面 并把request对象response对象传入
		request.getRequestDispatcher("goods_list.jsp").forward(request, response);
		
		
	
	
	}

}
