package com.max.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.max.domain.User;
import com.max.jdbcutil.JDBCUtil;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 设置编码
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		String sql="select * from user where username=? and password=?";
		User u=null;
		try {
			u = qr.query(sql, new BeanHandler<User>(User.class),username,password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(u!=null) {
			response.getWriter().write("登录成功");
//			保存用户数据 并存session域中
			HttpSession session = request.getSession();
			session.setAttribute("user", u);
			response.setHeader("refresh", "3;url=/14_Mystore/index.jsp"); 
		}else {
			response.getWriter().write("登录失败");
			response.setHeader("refresh", "3;url=/14_Mystore/login.jsp"); 
		}
		

	}

}
