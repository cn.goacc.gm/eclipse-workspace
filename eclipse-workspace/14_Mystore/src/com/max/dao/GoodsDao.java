package com.max.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.max.domain.Goods;
import com.max.jdbcutil.JDBCUtil;

public class GoodsDao {

	public List<Goods> finAllGoods() {
		
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		String sql="select * from goods";
		List<Goods> AllGoods=null;
		try {
			AllGoods= qr.query(sql, new BeanListHandler<Goods>(Goods.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return AllGoods;
	}

}
