<%@page import="com.max.domain.Goods"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title> </title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" href="css/common.css">
		<link rel="stylesheet" href="css/goods_list.css">
		<link rel="stylesheet" href="css/pageStyle.css">
		
	</head>
	<body>
		
<!-- 头部 -->
<!-- 通过jsp指令导入头部 -->
<%@include file="/header.jsp" %>

<!-- 热卖 -->	
<div id="hot_goods">
	<h3>热卖商品</h3>
	<div class="hot_goods_body">
		<ul>
		
		<c:if test="${!empty AllGoods}">
			<c:forEach items="${AllGoods }"  var="Goods">
			<li>
				<a href='#'>
					<img src="img/pimages/${Goods.image}" alt=""> 
					<p> ${Goods.name }</p>
					<i class="yuan">¥</i><span class='price'>${Goods.price }</span>
				</a>
			</li>
			</c:forEach>
		
		</c:if>
		
		<c:if test="${empty AllGoods}">
		<h1>没有商品</h1>
		</c:if>
		
		
		
		</ul>
	</div>
	<!-- 分页 -->
	<div id="page" class="page_div"></div>
</div>
	

	
<!-- 尾部-->
<!-- 通过jsp指令导入尾部 -->
<%@ include file="footer.jsp" %>


<!-- 引入gs -->
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/paging.js"></script>
<script>
    //分页
    $("#page").paging({
        pageNo:3,  /*当前选中的是哪一页*/
        totalPage: 15, /*共多少页*/
        totalSize: 300,/*共多少条记录*/
        callback: function(num) {
           console.log(num);  /*控制台输出显示当前页数*/
        }
    })
</script>
	</body>
</html>
