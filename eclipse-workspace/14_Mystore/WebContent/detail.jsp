<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" href="css/common.css"/>
		<link rel="stylesheet" href="css/detal.css"/>
	</head>
<body>
	
<!-- 通过jsp指令导入头部 -->
<%@include file="/header.jsp" %>
	 
<!-- 面包屑导航 -->
<div id="bread_crumb">
	<div class="bread_center">
		<a href="index.html">首页</a>
		<small>&gt</small>   <!-- 大于符号 -->
		<a href="index.html">运动户外</a>
		<small>&gt</small> 
		<a href="index.html">运动服饰</a>
		<small>&gt</small>  
		  
	</div>
</div>

<!-- 详情展示 -->
<div id="detail">
	<!-- 左侧 -->
	<div class="detail_img">
		<img src="img/goods/bigPic.png" >
	</div>
	<!-- 右侧 -->
	<div class="detail_price">
		<h3>小米 红米5A 全网通版 2GB+16GB 浅蓝色 移动联通电信4G手机 双卡双待</h3>
		<div class="goods_price">
				<p class="ori_price">原价：￥699.00 <em></em></p>
				<p>价格:<i class="yuan">¥</i><span class="price">599</span></p>
		</div>
		
		<div class="goods_count">
			购买数量: <input type="text" value="1" />
		</div>
		
		<div class="goods_buy mt50 ml5">
			<input type="submit" value="加入购物车" >
		</div>
	</div>
</div>
	
<!-- 商品介绍 -->
<div id="introduce">
	<h3>商品详情</h3>
	<div class="introduce_body">
		<img src="img/detail_pic1.png" >
		<img src="img/detail_pic2.png" >
	</div>
</div>

<!-- 通过jsp指令导入尾部 -->
<%@ include file="footer.jsp" %>


	
</body>

</html>
