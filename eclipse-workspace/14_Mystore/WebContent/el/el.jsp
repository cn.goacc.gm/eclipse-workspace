<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<% pageContext.setAttribute("key", "pageContextValue"); %>
<% request.setAttribute("key2 ", "requestValue"); %>
<% pageContext.setAttribute("key3", "sessionVaule", pageContext.SESSION_SCOPE);%>
<% pageContext.setAttribute("key4", "applicationValue", pageContext.APPLICATION_SCOPE); %>

${pageScope.key} 
<br>
${requestScope.key2 }
<br>
${sessionScope.key3 }
<br>
${applicationScope.key4}

<h1>--------</h1>
<!-- 范围：从小到大 -->
${key }

<!-- 判断是否为空 -->
${empty key}
${empty user}





</body>
</html>