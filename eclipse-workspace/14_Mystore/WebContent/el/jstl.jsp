<%@page import="java.util.HashMap"%>
<%@page import="com.max.domain.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<% request.setAttribute("key", 100); %>

<c:if test="${1== 1 }">
<h1>真的</h1>
</c:if>
 
 <c:if test="${key >50 }">
 <h1>大于50</h1>
 </c:if>
  <c:if test="${key <50 }">
 <h1>小于50</h1>
 </c:if>
 
 <!-- 实质是将值存放到pageScope域中 key名为i -->
 <c:forEach begin="0" end="5" var="i">
 	<%-- ${pageScope.i}   和下面一样 --%>
 	${i }<br>
 </c:forEach>
<hr>

 <!-- 列表的遍历 -->
 <% 
	 List<String> list=new ArrayList<String>();
	 list.add("aaa");
	 list.add("bbb");
	 list.add("ccc");
	 request.setAttribute("keylist", list);
 %>
 <c:forEach items="${keylist }" var="str">
 	  ${str }
 </c:forEach>
 
 <hr>
 <!-- 自定义列表遍历 -->
 <%
 	List<User> userList=new ArrayList<User>();
 	User u1= new User();
 	u1.setUsername("张三");
 	
 	User u2= new User();
 	u2.setUsername("李四");
 	
 	userList.add(u1);
 	userList.add(u2);
 	
 	request.setAttribute("userListKey", userList);
 %>
 <c:forEach items="${userListKey }" var="user">
 	${user.username}
 </c:forEach>

<hr>
 <!-- Map遍历 -->
<%
HashMap<String,String> map = new HashMap<String,String>();
	map.put("key1", "key1Values");
	map.put("key2", "key2Values");
	map.put("key3", "key3Values");
	request.setAttribute("mapKey", map);
%>

<c:forEach items="${mapKey }" var="entry">
	${ entry.key}:${entry.value }<br>
</c:forEach>
 
 
 
 
 
 
 
 
 
</body>
</html>