<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>登录界面</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" href="css/login.css"/>
	</head>
	<body>
<!-- 头部 -->
<div id="herder">
	<div class="h_centen">
		<img src="img/Myxq.jpg">
		<p>为确保您的账户安全及正常使用，依《网络安全法》规定，6月1日起会员需要绑定手机，如您还未绑定，请尽快完成，感谢您的理解和支持!</p>
	</div>
</div>

<!-- 中间 -->
<div id="login_body">
	<div class="login_b_centen">
	<div class="login_bg">
		<h4>用户登录</h4>
		<form action="/14_Mystore/LoginServlet" id="longin" method="POST">
			<!-- 用户名 -->
			<div class="userName">
				<span></span> <input type="text" name="username"/>
			</div>
			<!-- 密码 -->
			<div class="password">
				<span></span> <input type="password" name="password"/>
			</div>
			<!-- 按钮 -->
			<div class="longin_btn">
				<input type="submit" value="登录"/>
			</div>
			<div class="forgot_pwd">
				<a href="">忘记密码</a>
				<a href="">忘记用户名</a>
				<a href="">免费注册 </a>
			</div>
		</form>
		
	</div>
	</div>
	
</div>

<!-- 尾部 -->
<!-- 通过jsp指令导入尾部 -->
<%@ include file="footer.jsp" %>

	</body>
</html>
