<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link rel="stylesheet" href="css/hearder.css"/>
		<link rel="stylesheet" href="css/common.css"/>
	</head>
<body>

<!-- 头部 -->
<div id="hearder">
	<!-- 顶部 -->
	<div class="hearder_top">
	<div class="hearder_top_center">
		<div class="h_top_left">欢迎来到蚂蚁商城</div>
		
		<div class="h_top_right">
		<!-- 逻辑判断 -->
		<c:if test="${empty user }">
			<a href="login.jsp">登录</a>
			<a href="regist.jsp">免费注册</a>
		</c:if>
		<c:if test="${!empty user }">
			欢迎： ${ user.username}
			<a href="#">退出登录</a>
		</c:if>
		
			<a href="#">购物车</a>
			<a href="#">我的订单</a>
		</div>	
	</div>	
	</div>
	<!-- 中部 -->
	<div class="hearder_center"> 
		<div class="h_c_logo">
			<img src="img/log.png" >
		</div>
		<div class="h_c_scarch">
			<form action="#" >
				<input type="text" placeholder="请输入你要商品名称" class="t_input"/>
				<input type="submit" class="t_button" value="搜索" />
			</form>
			<div class="hot">
				<a href="#">新款短衣裙</a>
				<a href="#">四件套</a>
				<a href="#">潮流T恤</a>
				<a href="#">时尚短鞋</a>
				<a href="#">短裤半身裙</a>
			</div>
		</div>
		<div class="h_c_code">
			<img src="img/pcode.png" >
		</div>
	</div>
	<!-- 导航 -->
	<div class="nav">
		<ul>
			<li><a href="index.jsp">首页</a></li>
			<li><a href="/14_Mystore/GoodsServlet">电脑办公</a></li>
			<li><a href="#">家具家居</a></li>
			<li><a href="#">鲜果时光</a></li>
			<li><a href="#">图书音响</a></li>
			<li><a href="#">母婴孕婴</a></li>
		</ul>
	</div>
	
	
</div>


</body>
</html>