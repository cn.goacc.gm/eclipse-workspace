<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>注册界面</title>
		<link rel="stylesheet" href="css/common.css"/>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" href="css/regist.css"/>
	</head>
<body>
<!-- 头部 -->
<div id="reg_header">
	<div class="reg_h_center">
		<div class="reg_h_left">
			<img src="img/log.png" >
			<h4>欢迎注册</h4>
		</div>
		<div class="reg_h_right">
			<a href="login.jsp">请登录</a>
			<span>已有账号</span>
		</div>
			
	</div>
	
</div>

<!-- 表单内容 -->
<div id="reg_content">
	<div class="reg_content_left">
		<form action="/14_Mystore/RegistServlet" id="reg_form" method="POST">
			<div>
				<label>用户名</label>
				<input id="username" type="text"  placeholder="请输入用户名" name="username"/>
			</div>
			<div>
				<label>密码</label>
				<input id="pwd" type="password"  placeholder="请输入密码" name="password"/>
			</div>
			<div>
				<label>确认密码</label>
				<input id="pwd2" type="password"  placeholder="请输入密码"/>
			</div>
			<div>
				<label>电话</label>
				<input type="text"  placeholder="请输入电话" id="phone" name="phone"/>
			</div>
			<div class="check_box">
				<label>验证码</label>
				<input type="text" id="code" name="code"/>
				<img src="/14_Mystore/CheckCodeServlet" onclick="change(this)">
			</div>
			<div class="submit_btn">
				<input type="button" value="立即注册"  onclick="checkData()"/>
				
			</div>
			
			
		</form>
		
	</div>
	<div class="reg_content_right">
		<a href="#">
			<img src="img/reg_right.png" >
		</a>
	</div>
</div>

<!-- 通过jsp指令导入尾部 -->
<%@ include file="footer.jsp" %>


<script type="text/javascript">
	function change(obj) {
		obj.src="/14_Mystore/CheckCodeServlet?time="+new Date().getTime();
    }
	function checkData() {
        //获取用户名 密码 确认密码 
        var username = document.getElementById("username");
        var pwd = document.getElementById("pwd");
        var pwd2 = document.getElementById("pwd2");
        var phone = document.getElementById("phone");
        var code = document.getElementById("code");
       if (username.value == "") {
            alert("请输入用户名");
            return;
        }
        if (pwd.value == "") {
            alert("请输入密码");
            return;
        } 
        if (pwd2.value == "") {
            alert("请输入密码");
            return;
        }
        if(pwd.value!=pwd2.value){
            alert("两次密码不一致");
            return ;
        }
         if(phone.value == ""){
            alert("请输入电话");
            return ;
        }
        if(code.value == ""){
            alert("请输入验证码");
            return ;
        }else{
            var reg_form = document.getElementById("reg_form");
            reg_form.submit(); 
        }
         
		
	}
	
	

</script>

</body>
</html>
