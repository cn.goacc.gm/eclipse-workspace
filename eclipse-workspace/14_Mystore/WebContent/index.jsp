<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>首页</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link rel="stylesheet" href="css/index.css"/>
		<link rel="stylesheet" href="css/common.css"/>
	</head>
<body>
<!-- 通过jsp指令导入头部 -->
<%@include file="/header.jsp" %>

<!-- 广告 -->
<div id="ad">
	<img src="img/goods/ad.jpg" >
</div>

<!-- 秒杀 -->
<div id="ms">
			<div class="ms_top">
				<span>蚂蚁秒杀</span>
				<span>总有你想象不到的低价</span>
			</div>
			 
			<div class="ms_body">
				<ul>
					<li><a href="#">
							<img src="img/goods/good1.png" >
							<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
							<i class="yuan">¥</i><span class="price">599</span>
						</a>
					</li>
					<li><a href="#">
							<img src="img/goods/goods2.png" >
							<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
							<i class="yuan">¥</i><span class="price">599</span>
						</a>
					</li>
					<li><a href="#">
							<img src="img/goods/goods3.png" >
							<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
							<i class="yuan">¥</i><span class="price">599</span>
						</a>
					</li>
					<li><a href="#">
							<img src="img/goods/goods4.png" >
							<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
							<i class="yuan">¥</i><span class="price">599</span>
						</a></li>
					<li><a href="#">
							<img src="img/goods/goods5.png" >
							<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
							<i class="yuan">¥</i><span class="price">599</span>
						</a></li>
				</ul>
				
			</div>
			
</div>

<!-- 热卖 -->	
<div id="hot_goods">
	<h3>热卖商品</h3>
	<div class="hot_goods_body">
		<ul>
			<li><a href="#">
					<img src="img/goods/goods6.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods7.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods8.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods9.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods10.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods11.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
			<li><a href="#">
					<img src="img/goods/goods12.png" >
					<p> 小米（MI）小米净化器2智能家用卧室空气净化器除甲醛雾霾</p>
					<i class="yuan">¥</i><span class="price">599</span>
				</a></li>
		</ul>
	</div>
</div>

<!-- 通过jsp指令导入尾部 -->
<%@ include file="footer.jsp" %>



</body>
</html>
