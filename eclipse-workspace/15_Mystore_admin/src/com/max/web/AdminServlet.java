package com.max.web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.max.domain.Admin;
import com.max.service.AdminService;


@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			String username = request.getParameter("username");
			String password = request.getParameter("password");
			
				AdminService adminService = new AdminService();
				try {
					Admin admin = adminService.login(username,password);
					//把用户信息保存到session域中
					HttpSession session = request.getSession();
					session.setAttribute("admin", admin);
					
					//跳转到后台首页 
					//重定向 让浏览器跳转到指定位置
					response.sendRedirect(request.getContextPath()+"/admin/admin_index.jsp");
				
					
					
				} catch (Exception e) {
					//判断异常是否是系统异常还是自己写的 用户名或密码错误 异常
					if(e.getMessage().equals("用户名或密码错误")) {
						//跳转回登录界面 回显错误信息
						request.setAttribute("err", e.getMessage());
						//转发 服务器内部转发
						request.getRequestDispatcher("admin/admin_login.jsp").forward(request, response);
					}else {
						e.printStackTrace();
					}
				}
			
	}

}
