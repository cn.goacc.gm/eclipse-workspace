package com.max.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.max.dao.CategoryDao;
import com.max.domain.Category;
import com.max.domain.Goods;
import com.max.service.GoodsService;

/**
 * Servlet implementation class GoodsAddServlet
 */
@WebServlet("/GoodsAddServlet")
public class GoodsAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
	
		request.setCharacterEncoding("utf-8");

		Map<String, String[]> map = request.getParameterMap();
		Goods goods=new Goods();
		try {
			BeanUtils.populate(goods, map);
			goods.setImage("goods_001.png");
			
			GoodsService goodsService = new GoodsService();
			goodsService.addGoods(goods);
			
			request.getRequestDispatcher("/GoodsListServlet").forward(request, response);

		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
