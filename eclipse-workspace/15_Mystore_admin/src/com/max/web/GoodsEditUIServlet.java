package com.max.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.max.domain.Category;
import com.max.domain.Goods;
import com.max.service.CategoryService;
import com.max.service.GoodsService;

@WebServlet("/GoodsEditUIServlet")
public class GoodsEditUIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		GoodsService goodsService = new GoodsService();
		CategoryService categoryService = new CategoryService();
		try {
			Goods goods = goodsService.getGoodsWithId(id);
			List<Category> findCategory = categoryService.findCategory();
			request.setAttribute("goods", goods);
			request.setAttribute("Allcategory", findCategory);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		request.getRequestDispatcher("admin/edit.jsp").forward(request, response);
	
	}

}
