package com.max.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.max.domain.Goods;
import com.max.service.GoodsService;

@WebServlet("/GoodsEditServlet")
public class GoodsEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		Map<String, String[]> map = request.getParameterMap();
		GoodsService goodsService = new GoodsService();
		Goods goods = new Goods();
		try {
			BeanUtils.populate(goods, map);
			
			goodsService.updateGoods(goods);
			
			request.getRequestDispatcher("/GoodsListServlet").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		
	}

}
