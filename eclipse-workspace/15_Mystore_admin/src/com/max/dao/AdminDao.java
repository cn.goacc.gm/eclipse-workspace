package com.max.dao;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.max.domain.Admin;
import com.max.utlis.JDBCUtil;

public class AdminDao {

	public Admin checkAdmin(String name, String pwd) throws SQLException {
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		
		String sql=" select * from admin where username=? and password=?;";
		Admin admin=null;
		admin = qr.query(sql, new BeanHandler<Admin>(Admin.class),name,pwd);
		
		return admin;
		
 	}
}
