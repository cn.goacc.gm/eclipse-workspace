package com.max.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.max.domain.Category;
import com.max.utlis.JDBCUtil;

public class CategoryDao {

	public List<Category> getAllCategory() throws SQLException {
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		String sql="select * from category";
		
		return qr.query(sql, new BeanListHandler<Category>(Category.class));
	}

}
