package com.max.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.max.domain.Goods;
import com.max.utlis.JDBCUtil;

public class GoodsDao {
	private QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
	
	//1.查询所说商品
	public List<Goods> getAllGoods() throws SQLException{
		String sql="select * from goods";
		return qr.query(sql, new BeanListHandler<Goods>(Goods.class));
	}
	
	//2.添加商品到数据库
	public void addGoods(Goods goods) throws SQLException {
		String sql="insert into goods(name,price,image,gdesc,is_hot,cid) values(?,?,?,?,?,?)";
		qr.update(sql,goods.getName(),goods.getPrice(),goods.getImage(),goods.getGdesc(),goods.getIs_hot(),goods.getCid());
	}
	
	//3.根据id删除商品
	public void delGoods(int id) throws SQLException {
		String sql="delete from goods where id=?";
		qr.update(sql,id);
	}
	
	//4.更新商品 给我个商品 来去更新
	public void updateGoods(Goods goods) throws SQLException {
		String sql="update goods set name=?,price=?,image=?,gdesc=?,is_hot=?,cid=? where id=?";
		qr.update(sql,goods.getName(),goods.getPrice(),goods.getImage(),goods.getGdesc(),goods.getIs_hot()
				,goods.getCid(),  goods.getId());
	}

	//5.通过id获取商品信息
	public Goods getGoodsWithId(String id) throws SQLException {
		String sql="select * from goods where id=?";
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		return qr.query(sql, new BeanHandler<Goods>(Goods.class),id);
	}
	
	
}
