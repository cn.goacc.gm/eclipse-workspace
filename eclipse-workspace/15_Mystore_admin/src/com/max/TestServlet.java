package com.max;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TestServlet")
public class TestServlet extends BeasServlet {
	
	public String add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("add--");
		return "add.jsp";
		
	}
	public String del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("del---");
		return "del.jsp";
	}
	public String update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("update----");
		
		return "update.jsp";
	}
	
	

}
