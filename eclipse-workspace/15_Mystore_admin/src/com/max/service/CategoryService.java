package com.max.service;

import java.sql.SQLException;
import java.util.List;

import com.max.dao.CategoryDao;
import com.max.domain.Category;

public class CategoryService {

	public List<Category> findCategory() throws SQLException {
		CategoryDao categoryDao = new CategoryDao();
		return categoryDao.getAllCategory();
		
		
	}

}
