package com.max;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TestServlet2")
public class TestServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
 	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parameter = request.getParameter("action");
		String desPath=null;
		
		 if(parameter.equals("add")) {
			 desPath = add(request,response);
		 }else if(parameter.equals("del")) {
			 desPath = del(request,response);
		 }else if(parameter.equals("update")) {
			 desPath = update(request,response);
		 }
		 
		 if(desPath!=null) {
			 request.getRequestDispatcher(desPath).forward(request, response );
		 }
	}
	
	protected String add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("add--");
		return "add.jsp";
		
	}
	protected String del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("del---");
		return "del.jsp";
	}
	protected String update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("update----");
		
		return "update.jsp";
	}
	
	

}
