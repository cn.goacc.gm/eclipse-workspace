package com.max.request;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

/**
 * Servlet implementation class RequestServlet
 */
@WebServlet("/RequestServlet2")
public class RequestServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		
		/*Map<String, String[]> map = request.getParameterMap();
		for (Map.Entry<String, String[]> v : map.entrySet()) {
			System.out.println(v.getKey()+":"+Arrays.toString(v.getValue()));
		}*/
		Map<String, String[]> map = request.getParameterMap();
		User u=new User();
		try {
			BeanUtils.populate(u, map);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		System.out.println(u);
		
		
		
	
	}

}
