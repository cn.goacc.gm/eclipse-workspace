package com.max.request;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RequestServlet
 */
@WebServlet("/RequestServlet")
public class RequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("RequestServlet");
		
//获取请求行
		//获取请求方式 (get or post)
		String method = request.getMethod();
		System.out.println(method);
		//获取请求资源
		StringBuffer url = request.getRequestURL();  /* http://localhost:8080/10_Request/RequestServlet */
		String uri = request.getRequestURI();		/* /10_Request/RequestServlet */
		System.out.println(url);
		System.out.println(uri);
		//获取get请求参数
		String queryString = request.getQueryString();  /* username=Aaa&pwd=123 */
		System.out.println(queryString);
		//获取当前web名称
		String contextPath = request.getContextPath();	/* /10_Request */
		System.out.println(contextPath);
//获取请求头
		//获取所有请求头
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()) {
			String nextElement = headerNames.nextElement();
			System.out.println("name = "+nextElement);
		}
		//根据请求头名称获取相应的值
		String headervalue = request.getHeader("referer");
		System.out.println("referer :"+headervalue);
		//获取所有请求头和请求内容
		Enumeration<String> headerNames2 = request.getHeaderNames();
		while(headerNames2.hasMoreElements()) {
			String nextElement = headerNames2.nextElement();
			String header_value = request.getHeader(nextElement);
			System.out.println(nextElement+":"+header_value);
		}
		
//获取请求体
		//获取一个值
		String parameter = request.getParameter("username");
		System.out.println(parameter);
		//获取多个值
		String[] parameterValues = request.getParameterValues("hobby");
		System.out.println(Arrays.toString(parameterValues));
		//获取所有请求名称
		Enumeration<String> names = request.getHeaderNames();
		while(names.hasMoreElements()) {
			String nextElement = names.nextElement();
			System.out.println(nextElement);
		}
		System.out.println("----------------");
		
		//获取所有名称和值
		Map<String, String[]> parameterMap = request.getParameterMap();
		for (Map.Entry<String, String[]> map: parameterMap.entrySet()) {
			System.out.println(map.getKey()+"="+ Arrays.toString(map.getValue()) );
		}
		
		
	
	}

}
