package com.max.request;

import java.util.Arrays;

public class User {

	private String username;
	private String pwd;
	private String gener;
	private String[] hobby;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getGener() {
		return gener;
	}
	public void setGener(String gener) {
		this.gener = gener;
	}
	public String[] getHobby() {
		return hobby;
	}
	public void setHobby(String[] hobby) {
		this.hobby = hobby;
	}
	@Override
	public String toString() {
		return "User [username=" + username + ", pwd=" + pwd + ", gener=" + gener + ", hobby=" + Arrays.toString(hobby)
				+ "]";
	}
	

}
