package com.max.resolver;

import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PareClass {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		//创建解析器工厂
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//通过解析工厂得到解析器
		DocumentBuilder db = dbf.newDocumentBuilder();
		//通过解析器得到dcument对象
		Document doc = db.parse("src/com/max/xml/student.xml"); 
		//获取具体的节点内容
		NodeList list = doc.getElementsByTagName("name");  //NodeList列表
		Node name = list.item(0); //获取第一个节点   导包注意 org.w3c.dom.Node;
		System.out.println(name.getTextContent());
		
	}
}
