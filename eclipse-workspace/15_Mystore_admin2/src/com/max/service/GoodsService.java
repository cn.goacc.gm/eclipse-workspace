package com.max.service;

import java.sql.SQLException;
import java.util.List;

import com.max.dao.GoodsDao;
import com.max.domain.Goods;

public class GoodsService {
	private GoodsDao goodsDao = new GoodsDao();
	
	
	public List<Goods> getAllGoods() throws Exception {
		return  goodsDao.getAllGoods();
		
	}

	public void deleteGoods(String id) throws SQLException {
		
		goodsDao.delGoods(Integer.parseInt(id));
	}

	public void addGoods(Goods goods) throws SQLException {
		
		goodsDao.addGoods(goods);
		
	}

	public Goods getGoodsWithId(String id) throws SQLException {
		
		
		return goodsDao.getGoodsWithId(id);
		
	}

	public void updateGoods(Goods goods) throws SQLException {

		goodsDao.updateGoods(goods);
		
	}
	
	
	

}
