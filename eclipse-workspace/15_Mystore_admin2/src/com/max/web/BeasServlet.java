package com.max.web;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class BeasServlet
 */
@WebServlet("/BeasServlet")
public class BeasServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		
		String parameter = request.getParameter("action");
		System.out.println(parameter);
		
		try {
			Class clazz = this.getClass();
			Method method = clazz.getMethod(parameter, HttpServletRequest.class,HttpServletResponse.class);
		
			String desPath = (String) method.invoke(this, request,response);
			
			 if(desPath!=null) {
				 request.getRequestDispatcher(desPath).forward(request, response );
			 }
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		
		
		
	}

}
