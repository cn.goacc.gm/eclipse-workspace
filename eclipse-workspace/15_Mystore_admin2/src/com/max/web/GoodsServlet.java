package com.max.web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;

import com.max.domain.Category;
import com.max.domain.Goods;
import com.max.service.CategoryService;
import com.max.service.GoodsService;

@WebServlet("/GoodsServlet")
public class GoodsServlet extends BeasServlet {

	// 获取所有商品
	public String getListGoods(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		GoodsService goodsService = new GoodsService();
		try {
			List<Goods> allGoods = goodsService.getAllGoods();
			// 将集合反转
			Collections.reverse(allGoods);

			// 将数据写到request域
			request.setAttribute("allGoods", allGoods);

			return "admin/main.jsp";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	//添加商品
	public String addGoods(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");

		Map<String, String[]> map = request.getParameterMap();
		Goods goods = new Goods();
		try {
			BeanUtils.populate(goods, map);
			goods.setImage("goods_001.png");

			GoodsService goodsService = new GoodsService();
			goodsService.addGoods(goods);
			return "/GoodsServlet?action=getListGoods";

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 删除商品
	public String DelGoods(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");

		GoodsService goodsService = new GoodsService();
		try {
			goodsService.deleteGoods(id);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return "/GoodsServlet?action=getListGoods";
	}

	
	//更新商品UI
	public String GoodsEditUI(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");
		GoodsService goodsService = new GoodsService();
		CategoryService categoryService = new CategoryService();
		try {
			Goods goods = goodsService.getGoodsWithId(id);
			List<Category> findCategory = categoryService.findCategory();
			request.setAttribute("goods", goods);
			request.setAttribute("Allcategory", findCategory);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return "admin/edit.jsp";
	}
	
	
	//更新商品
	public String GoodsEdit(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8");
		
		//!!!!!!!!!!
		String parameter = request.getParameter("name");
		System.out.println("------"+parameter);
		
		Map<String, String[]> map = request.getParameterMap();
		GoodsService goodsService = new GoodsService();
		Goods goods = new Goods();
		try {
			BeanUtils.populate(goods, map);
			
			goodsService.updateGoods(goods);
			
			
			return "/GoodsServlet?action=getListGoods";
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
		
	}
	
	public String addGoodsUI(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CategoryService categoryService = new CategoryService();
		try {
			List<Category> allCategory = categoryService.findCategory();
			request.setAttribute("allCategory", allCategory);
			
			return "admin/add.jsp";
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
		
		
		
	}
	

}
