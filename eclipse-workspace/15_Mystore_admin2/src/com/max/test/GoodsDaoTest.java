package com.max.test;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.max.dao.GoodsDao;
import com.max.domain.Goods;

public class GoodsDaoTest {
	private GoodsDao dao=new GoodsDao();
	@Test
	public void getAllGoodsDaoTest() throws SQLException {
		List<Goods> allGoods = dao.getAllGoods();
		System.out.println(allGoods);
	} 
	
	@Test 
	public void addGoodsTest() throws SQLException {
		Goods goods=new Goods();
		goods.setName("测试商品名称");
		goods.setPrice(100.0);
		goods.setImage("测试图片.png");
		goods.setGdesc("商品描述");
		goods.setIs_hot(0);
		goods.setCid(4);
		dao.addGoods(goods);
	}
	@Test
	public void delGoodsTest() throws SQLException {
		dao.delGoods(19);
	}
	
	@Test
	public void updateGoodsTest() throws SQLException {
		Goods goods=new Goods();
		goods.setId(22);
		goods.setName("更新商品名称");
		goods.setPrice(200.0);
		goods.setImage("更新图片.png");
		dao.updateGoods(goods);
	}
	
	@Test
	public void getGoodsWithIdTest() throws SQLException{
		Goods goods = dao.getGoodsWithId("4");
		System.out.println(goods);
	}

}
