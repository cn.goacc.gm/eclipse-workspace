package com.max.jdbc.test;

import java.util.List;

import org.junit.Test;

import com.max.jdbc.dao.IStudentDao;
import com.max.jdbc.dao.impl.StudentDaoImpl;
import com.max.jdbc.domain.Student;

public class StudentDaoTest {
	
	//单元测试
	@Test
	public void save() {
		IStudentDao isd=new StudentDaoImpl();
		Student stu = new Student();
		stu.setName("王五jkjkj");
		stu.setAge(24);
		isd.save(stu);
	}
	@Test
	public void delete() {
		IStudentDao isd=new StudentDaoImpl();
		isd.delete(8);
	} 
	@Test
	public void update() {
		IStudentDao isd=new StudentDaoImpl();
		Student stu = new Student();
		stu.setName("单元klklklkk");
		stu.setAge(20);
		isd.update(1,stu);
	}
	@Test 
	public void get() {
		IStudentDao isd=new StudentDaoImpl();
		Student stu = isd.get(1);
		System.out.println(stu);
	}
	@Test
	public void getAll() {
		IStudentDao isd=new StudentDaoImpl();
		List<Student> all = isd.getAll();
		System.out.println(all.toString());
	}
	@Test
	public void getCount() {
		IStudentDao is = new StudentDaoImpl();
		System.out.println(is.getCount());
	}
	
	
	
}
