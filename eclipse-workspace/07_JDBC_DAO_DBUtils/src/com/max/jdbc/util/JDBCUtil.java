package com.max.jdbc.util;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;


public class JDBCUtil {
	public static DataSource ds = null;   

	static {
		try {
			//加载配置文件
			Properties p=new Properties();
			FileInputStream in = new FileInputStream("resource/db.properties");
			p.load(in);
//			ds = BasicDataSourceFactory.createDataSource(p);  //dbcp连接池   导包注意 sql.DataSource;
			ds = DruidDataSourceFactory.createDataSource(p);   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//返回(数据源)DataSource对象
	public static DataSource getDataSource() {
		return ds;
	}
	
	public static Connection getconn() {
		// 2.连接数据库
		try {
			return ds.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	//释放资源
	public static void Close(Connection conn, Statement st, ResultSet rs) {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (rs != null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (st != null)
				st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	

}
