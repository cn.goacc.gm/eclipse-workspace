package com.max.jdbc.util;

import org.apache.commons.dbutils.QueryRunner;

public class CRUDTemplate {
	public static QueryRunner getQueryRunner() {
		return new QueryRunner(JDBCUtil.getDataSource());
	}

}
