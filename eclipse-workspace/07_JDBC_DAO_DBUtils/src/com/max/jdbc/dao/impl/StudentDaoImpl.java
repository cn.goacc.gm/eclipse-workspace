package com.max.jdbc.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.max.jdbc.dao.IStudentDao;
import com.max.jdbc.domain.Student;
import com.max.jdbc.util.CRUDTemplate;

public class StudentDaoImpl implements IStudentDao {

	@Override
	public void save(Student stu) {
		String sql = "insert into student (name,age) values(?,?)";
		try {
			CRUDTemplate.getQueryRunner().update(sql,stu.getName(),stu.getAge());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(int id, Student stu) {
		String sql = "update student set name=?,age=? where id=?";
		try {
			CRUDTemplate.getQueryRunner().update(sql,stu.getName(),stu.getAge(),id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(int id) {
		String sql = "delete from student where id=?";
		
		try {
			CRUDTemplate.getQueryRunner().update(sql,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Student get(int id) {
		String sql = "select * from student where id=?";
		try {
			return CRUDTemplate.getQueryRunner().query(sql, new BeanHandler<Student>(Student.class),id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Student> getAll() {
		String sql = "select * from student";
		try {
			return CRUDTemplate.getQueryRunner().query(sql, new BeanListHandler<Student>(Student.class));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Integer getCount() {
		String sql="select count(*) from student";
		try {
			return CRUDTemplate.getQueryRunner().query(sql,new CountResultSetHandler());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	class CountResultSetHandler implements ResultSetHandler<Integer>{

		@Override
		public Integer handle(ResultSet rs) throws SQLException {
			if(rs.next()) {
				return rs.getInt("count(*)");
			}
			return null;
		}
		
	}
	

}
