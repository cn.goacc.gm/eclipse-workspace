package com.max.seesion;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SeesionServlet
 */
@WebServlet("/SeesionServlet")
public class SeesionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
			HttpSession session = request.getSession();
			
			//销毁session对象
//			session.invalidate(); 
			
			//session持久化时间
			/*关闭浏览器 取不到session数据的原因是默认的存储sessionId的cookie是会话级别的 浏览一关就会话就结束了  */
			Cookie cookie = new Cookie("JSESSIONID",session.getId());
			cookie.setMaxAge(60*2);
			cookie.setPath("/Cookie-Seesion");
			response.addCookie(cookie);
			session.setAttribute("key", "kkkkkk");
	
			
	
	}

}
