package com.max.cookie;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieServlet
 */
@WebServlet("/CookieServlet2")
public class CookieServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		Cookie cookie = new Cookie("key","value22");
		
		
		//获取cookie信息
		//删除cookie信息
//		cookie.setMaxAge(0);
		
//		response.addCookie(cookie);
		
		//获取cookie信息
		Cookie[] cookies = request.getCookies();
		if(cookies!=null) {
			for (Cookie cookie : cookies) {
				if(cookie.getName().equals("key")) {
					System.out.println(cookie.getValue());
				}
			}
			
			
		}
		
	
	}

}
