package com.max.cookie;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Lastcookie
 */
@WebServlet("/Lastcookie")
public class Lastcookie extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-ddhh:mm:ss");
		String format = sdf.format(date);
		System.out.println(format);
		
		Cookie c = new Cookie("listTime", format);
		response.addCookie(c);
	
		//获取
		String lastTime=null;
		Cookie[] cookies = request.getCookies();
		if(cookies!=null) {
			for (Cookie cookie2 : cookies) {
				if(cookie2.getName().equals("listTime")) {
					lastTime=cookie2.getValue();
					System.out.println(lastTime);
				}
			}
		}
		if(lastTime!=null) {
			response.getWriter().write("你是上次登入的时间:"+lastTime);
		}else {
			response.getWriter().write("你是第一次登录");
		}
		
		
	
	}

}
