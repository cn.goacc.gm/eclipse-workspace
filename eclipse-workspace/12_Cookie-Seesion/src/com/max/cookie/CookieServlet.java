package com.max.cookie;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CookieServlet")
public class CookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookie = new Cookie("key","value22");
		//设置cookie持久化时间
//		cookie.setMaxAge(30);
		
		//设置携带路径
		//只有访问该servlet才能携带信息
		//默认在同一路径下都可以访问cookie信息
		
		//指明某个servlet携带数据
//		cookie.setPath("/Cookie-Seesion/CookieServlet");
		
		//指明在该web工程下携带数据
//		cookie.setPath("/Cookie-Seesion");
		

		
		response.addCookie(cookie);
		
		
		
		
	
	}

}
