package com.max.dom4j_resolver;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import javax.swing.plaf.OptionPaneUI;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class dom4jTest {
	public static void main(String[] args) throws Exception  {
		text2();
	}
	
	static void text2() throws Exception {
		SAXReader saxReader=new SAXReader();
		Document document=saxReader.read("src/com/max/xml/student.xml");
		Element rootElement = document.getRootElement(); 
		Element addAttribute = rootElement.addElement("student").addAttribute("number", "004");
		addAttribute.addElement("name").addAttribute("ID", "002").setText("薛定谔");
		addAttribute.addElement("age").setText("45");
		addAttribute.addElement("sex").setText("男");
		
		//写到文件
		OutputFormat Format =OutputFormat.createPrettyPrint();
		Writer wr=new OutputStreamWriter(new FileOutputStream("src/com/max/xml/student.xml"),"UTF-8");
		XMLWriter xmlWriter=new XMLWriter(wr,Format);
		xmlWriter.write(document);
		xmlWriter.close();
		
	}
	
	
	//查询
	static void text1() throws Exception{		//创建SAXReader对象
		SAXReader reader = new SAXReader();
		Document doc = reader.read("src/com/max/xml/student.xml");
		//获取根元素
		Element re = doc.getRootElement();
		//获取根元素下的所有元素
		List<Element> list = re.elements("student");
		for(Element item:list) {
			//			Element element = item.element("name");
			//			element.getText();
			//		-------------------------------			
			//			item.elementText("name");   和上面一样
			//			item.attributeValue("number") 获取属性值
			//			item.getName()   	获取标签名
		}

	}

}
