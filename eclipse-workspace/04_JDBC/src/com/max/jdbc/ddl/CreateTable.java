package com.max.jdbc.ddl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTable {

	public static void main(String[] args) throws Exception {
		//1.加载驱动
				//把com.mysql.jdbc.Driver这份字节码加载进JVM
				//当一份字节码被加载到JVM时，就会执行该字节码中的静态代码块
				Class.forName("com.mysql.jdbc.Driver");
			
				//2.获取连接对象
				//连接地址
				String url="jdbc:mysql://127.0.0.1:3306/jdbc_db";
				
				String user="root";
				String password="li767787498";
				Connection conn = DriverManager.getConnection(url, user, password);
				//3.编写sql语句
				String sql="create table stu(id int,name varchar(20),age int)";
				Statement st = conn.createStatement();  //创建Statement接口
				//4.执行sql语句
				int eu = st.executeUpdate(sql);
				System.out.println(eu);
				//释放资源
				st.close();
				conn.close();
				

	}

}
