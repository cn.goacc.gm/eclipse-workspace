package com.max.jdbc.conn;


import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionClass {
	public static void main(String[] args) throws Exception {
		//1.加载驱动
		//把com.mysql.jdbc.Driver这份字节码加载进JVM
		//当一份字节码被加载到JVM时，就会执行该字节码中的静态代码块
		Class.forName("com.mysql.jdbc.Driver");
	
		//2.获取连接对象
		//连接地址
		String url="jdbc:mysql://127.0.0.1:3306/my_test";
		
		String user="root";
		String password="li767787498";
		Connection conn = DriverManager.getConnection(url, user, password);
		System.out.println(conn);
//		Thread.sleep(500000);
		
		
	}
}
