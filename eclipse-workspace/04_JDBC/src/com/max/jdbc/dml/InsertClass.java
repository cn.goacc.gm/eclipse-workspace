package com.max.jdbc.dml;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertClass {

	public static void main(String[] args)  {
		Connection conn=null;
		Statement st=null;
		
		try {
			//加载注册驱动
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_db", "root", "li767787498");
			st= conn.createStatement();
			 
			String sql="insert into stu values(3,'ww',23)";
			int row = st.executeUpdate(sql);
			System.out.println(row);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(st != null)
					st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		

	}

}
