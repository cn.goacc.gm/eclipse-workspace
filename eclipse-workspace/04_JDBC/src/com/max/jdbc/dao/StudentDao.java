package com.max.jdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class StudentDao {
	public void save(Student stu)  {
		Connection conn=null;
		Statement st=null;
		
		try {
			//加载注册驱动
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_db", "root", "li767787498");
			st= conn.createStatement();
			Integer id=stu.id;
			String name=stu.name;
			Integer age=stu.age;
			
			String sql="insert into stu values("+id+",'"+name+"',"+age+")";
			System.out.println(sql);
			int row = st.executeUpdate(sql);
			System.out.println(row);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(st != null)
					st.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}

}
