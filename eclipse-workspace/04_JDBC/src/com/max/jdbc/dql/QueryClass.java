package com.max.jdbc.dql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class QueryClass {
	public static void main(String[] args) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_db", "root", "li767787498");

		String sql="select * from emp";
		Statement st = conn.createStatement();
		ResultSet res = st.executeQuery(sql);
		while(res.next()) {
			int empno = res.getInt("empno");
			String name = res.getString("name");
			String job = res.getString("job");
			System.out.println(empno+" "+name+" "+job);
		}
		
		st.close();
		conn.close();



	}
	//取多行数据
	public void test2() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_db", "root", "li767787498");

		String sql="select * from emp where name='鲁班'";
		Statement st = conn.createStatement();
		ResultSet res = st.executeQuery(sql);
		if(res.next()) {
			int empno = res.getInt("empno");
			String name = res.getString("name");
			String job = res.getString("job");
			System.out.println(empno+" "+name+" "+job);
		}
		st.close();
		conn.close();
	}


	//	取单行数据
	public void test1() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");

		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_db", "root", "li767787498");

		String sql="select count(*) as total from emp";
		Statement st = conn.createStatement();
		ResultSet res = st.executeQuery(sql);
		if(res.next()) {
			//			int count = res.getInt(1);  //通过下标来取
			int count = res.getInt("total");  //通过字段名来取 （查询时推荐as起个别名）
			System.out.println(count);
		}
		st.close();
		conn.close();
	}
}
