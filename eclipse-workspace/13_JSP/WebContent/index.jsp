<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- 静态包含 -->
<%-- <%@ include file="/header.jsp" %>
<h1>主体</h1>
<%@ include file="/footer.jsp" %> --%>

<!-- 动态包含 -->
<%-- <jsp:include page="/header.jsp"></jsp:include>
<h1>主体</h1>
<jsp:include page="/footer.jsp"></jsp:include> --%>

<!-- 请求转发 -->
<jsp:forward page="/footer.jsp"></jsp:forward>

</body>
</html>