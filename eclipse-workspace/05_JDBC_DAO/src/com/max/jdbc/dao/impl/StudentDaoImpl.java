package com.max.jdbc.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.max.jdbc.dao.IStudentDao;
import com.max.jdbc.domain.Student;
import com.max.jdbc.util.JDBCUtil;

public class StudentDaoImpl implements IStudentDao {
	private Connection conn = null;
	private PreparedStatement ps = null;
	private ResultSet rs = null;

	@Override
	public void save(Student stu) {
		try {
			conn = JDBCUtil.getconn();
			String sql = "insert into student (name,age) values(?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, stu.getName());
			ps.setInt(2, stu.getAge());
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, null); // 向下转型
		}

	}

	@Override
	public void update(int id, Student stu) {
		try {
			conn = JDBCUtil.getconn();
			String sql = "update student set name=?,age=? where id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, stu.getName());
			ps.setInt(2, stu.getAge());
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, null);
		}

	}

	@Override
	public void delete(int id) {
		try {
			conn = JDBCUtil.getconn();
			String sql = "delete from student where id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, null);
		}

	}

	@Override
	public Student get(int id) {
		try {
			conn = JDBCUtil.getconn();
			String sql = "select * from student where id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				Student stu = new Student();
				stu.setId(rs.getInt("id"));
				stu.setName(rs.getString("name"));
				stu.setAge(rs.getInt("age"));
				return stu;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, rs);
		}
		return null;
	}

	@Override
	public List<Student> getAll() {
		try {
			conn = JDBCUtil.getconn();
			String sql = "select * from student";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			List<Student> list = new ArrayList<Student>();
			while (rs.next()) {
				Student stu = new Student();
				stu.setId(rs.getInt("id"));
				stu.setName(rs.getString("name"));
				stu.setAge(rs.getInt("age"));
				list.add(stu);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, rs);
		}
		return null;
	}

}
