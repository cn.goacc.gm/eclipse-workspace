package com.max.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.max.jdbc.util.JDBCUtil;

public class TransactionTest {

	public static void main(String[] args) throws Exception {

		//判断钱是否够
		Connection conn = JDBCUtil.getconn();
		String sql="select * from account where name=? and money > ?;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, "ls");
		ps.setInt(2, 1000);
		ResultSet rs = ps.executeQuery();
		if(!rs.next()) {
			System.out.println("没钱了");
			return ;
		}
		try {
			//开启事务 （取消自动提交）
			conn.setAutoCommit(false);
			//减少
			sql="update account set money = money - ? where name = ?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, 1000);
			ps.setString(2, "ls");
			ps.executeUpdate();
			
			//写了异常
			int a = 1/0;
			
			//增加
			sql="update account set money = money + ? where name = ?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, 1000);
			ps.setString(2,"ww");
			ps.executeUpdate();
			conn.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			conn.rollback();  //回滚  （释放资源）
		}
		
		
		JDBCUtil.Close(conn, ps, rs);
	}

}
