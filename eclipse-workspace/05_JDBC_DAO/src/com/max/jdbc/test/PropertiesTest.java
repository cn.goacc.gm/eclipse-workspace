package com.max.jdbc.test;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertiesTest {
	public static void main(String[] args) throws Exception {
		//读取配置文件
		Properties p =new Properties();
		FileInputStream is = new FileInputStream("resource/db.properties");
		p.load(is);
		System.out.println(p.getProperty("url"));
		
	}

}
