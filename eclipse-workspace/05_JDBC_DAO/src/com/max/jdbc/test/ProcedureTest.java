package com.max.jdbc.test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import com.max.jdbc.util.JDBCUtil;

public class ProcedureTest {
	public static void main(String[] args) {
		//调用mysql里的存储过程
		
		//调用存储过程
		/*Connection conn = JDBCUtil.getconn();
		try {
			CallableStatement cs = conn.prepareCall("{ call getstu(?)}");
			cs.setString(1, "李四");
			ResultSet rs = cs.executeQuery();
			if(rs.next()) {
				Student stu=new Student();
				stu.setId(rs.getInt("id"));
				stu.setName(rs.getString("name"));
				stu.setAge(rs.getInt("age"));
				System.out.println(stu);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//调用有输出参数的存储过程
		Connection conn = JDBCUtil.getconn();
		try {
			CallableStatement cs = conn.prepareCall("{ call getName(?,?)}");
			cs.setInt(1, 6);
			cs.registerOutParameter(2, Types.VARCHAR); //设置输出参数类型
			cs.execute();
			String string = cs.getString(2);
			System.out.println(string);
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
