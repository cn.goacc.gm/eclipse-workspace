package com.max.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.dbcp.BasicDataSource;

public class DBCPTest {
	public static void main(String[] args) throws Exception {
		String url = "jdbc:mysql://127.0.0.1:3306/jdbc_db?rewriteBatchedStatements=true";
		String user = "root";
		String password = "li767787498";
		String driverName = "com.mysql.jdbc.Driver";

		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(driverName);
		ds.setUrl(url);
		ds.setUsername(user);
		ds.setPassword(password);
		Connection conn = ds.getConnection();
		
		String sql="select * from account";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String name = rs.getString("name");
			int money = rs.getInt("money");
			System.out.println(name+","+money);
			
		}
		
	}

}
