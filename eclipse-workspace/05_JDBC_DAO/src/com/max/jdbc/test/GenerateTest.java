package com.max.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.max.jdbc.util.JDBCUtil;

public class GenerateTest {
	public static void main(String[] args) throws Exception {
		//创建的时候获取主键
		Connection conn = JDBCUtil.getconn();
		String sql="insert into student (name,age) values(?,?)";
		PreparedStatement ps = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);  //设置可以获取主键  注意导包 sql包
		ps.setString(1, "李四");
		ps.setInt(2, 23);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys(); 		//获取主键
		if(rs.next()) {
			String key = rs.getString(1);
			System.out.println(key);
		}
		
	}

}
