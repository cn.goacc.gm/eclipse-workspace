package com.max.jdbc.test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.max.jdbc.util.JDBCUtil;

public class BlobTest {
	public static void main(String[] args) throws Exception {

		//存一张图片到数据库中
		/*Connection conn = JDBCUtil.getconn();
		String sql="insert into student (img) values (?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setBlob(1, new FileInputStream("/Users/lilei/Desktop/imhh.png"));
		ps.executeUpdate();
		JDBCUtil.Close(conn, ps, null);*/
		
		//从数据库中获取
		Connection conn = JDBCUtil.getconn();
		String sql="select * from student where id=?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, 1);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			Blob blob = rs.getBlob("img");
			InputStream is = blob.getBinaryStream();
			Files.copy(is,Paths.get("/Users/lilei/Desktop/img_pp.png"));
		}
		JDBCUtil.Close(conn, ps, rs);
	}
}
