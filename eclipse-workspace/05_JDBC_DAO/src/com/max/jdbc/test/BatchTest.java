package com.max.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.max.jdbc.util.JDBCUtil;

public class BatchTest {
	public static void main(String[] args) throws Exception {
		//先在url上添加	rewriteBatchedStatements=true    1397   102
		
		
		Connection conn = JDBCUtil.getconn();

		String sql = "insert into student (name,age) values (?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		long begin=System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			ps.setString(1, "批处理"+i);
			ps.setInt(2, 1+i);
			ps.addBatch();		//sql语句添加
//			ps.executeUpdate();
		}
		ps.executeBatch();		//执行批处理
		long end=System.currentTimeMillis();
		System.out.println("花费时间"+(end-begin));
		JDBCUtil.Close(conn, ps, null);
	}

}
