package com.max.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.max.jdbc.util.JDBCUtil;

public class loginTest {
	public static void main(String[] args) {
		loginIs("001", "1234");
	}

	public static int loginIs(String name,String psd) {
		Connection conn = JDBCUtil.getconn();
		String sql="select * from user where name=? and psd=?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, psd);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				System.out.println(rs.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return 0;
	}
	
}
