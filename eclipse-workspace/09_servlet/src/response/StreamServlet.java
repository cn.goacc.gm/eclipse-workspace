package response;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.corba.se.impl.encoding.CodeSetConversion.BTCConverter;

/**
 * Servlet implementation class StreamServlet
 */
@WebServlet("/StreamServlet")
public class StreamServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = StreamServlet.class.getClassLoader().getResource("a.txt").getPath();
		FileInputStream is=new FileInputStream(path);
		byte[] b= new byte[6];
		int len=0;
		while((len=is.read(b)) !=-1) {
			System.out.println(len);
			System.out.println(new String(b, 0, len));
		}
		
		is.close();
		
	
	}

}
