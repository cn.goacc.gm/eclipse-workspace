package response;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ResponseServlet
 */
@WebServlet("/ResponseServlet")
public class ResponseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//设置写入缓存区为utf-8 (可以省略)
//		resp.setCharacterEncoding("utf-8");
		//设置浏览解析编码
//		resp.setHeader("Content-type", "text/html;charset=utf-8");
		//封装的Content-type
		resp.setContentType("text/html;charset=utf-8");
		
		//响应行   设置状态码
		resp.setStatus(302);
		
		//添加响应头
		resp.addHeader("key", "值");
		resp.addIntHeader("keyint", 100);
		resp.addDateHeader("time", new Date().getTime());
		
		//修改响应头
		resp.setHeader("key", "修改—ok");
		
		//重定向 注意要修改响应码
//		resp.setHeader("location", "/09_servlet/LocationServlet");
//		resp.sendRedirect("/09_servlet/LocationServlet");  //封装后的设置重定向
//		resp.setHeader("refresh", "3;url=http://www.baidu.com");  //定时重定向 
		
		//响应体
		//写字符
		resp.getWriter().write("Tomcat ");
		//如果字符里有html标签浏览器会帮你解析
		resp.getWriter().write("<h1>cc</h1>");
		resp.getWriter().write("汤姆猫");
		
	
	}

}
