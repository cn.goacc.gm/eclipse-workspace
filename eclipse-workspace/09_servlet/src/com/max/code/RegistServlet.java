package com.max.code;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegistServlet
 */
@WebServlet("/RegistServlet")
public class RegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("code");

		String code = (String) this.getServletContext().getAttribute("checkCode");
		
		response.setContentType("text/html;charset=utf-8");
		
		if (name.equals(code)) {
			response.getWriter().write("登录成功");
		} else {
			response.getWriter().write("验证码错误");
			response.setHeader("refresh", "3;url=/09_servlet/code.html");
		}

	}

}
