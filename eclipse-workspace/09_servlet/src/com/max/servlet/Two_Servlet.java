package com.max.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Two_Servlet")
public class Two_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		ServletContext context = config.getServletContext();
		String vale = (String) context.getAttribute("key");
		System.out.println(vale);
	
	
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
	
	
	}

}
