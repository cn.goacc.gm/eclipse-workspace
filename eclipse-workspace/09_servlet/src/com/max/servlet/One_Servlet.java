package com.max.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class One_Servlet
 */
@WebServlet("/One_Servlet")
public class One_Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		//注意 init()方法 是在Servlet创建时先执行的方法
		//注意 在其他方法中获取Context对象其本质还是通过init方法来获取  但如果重写init方法将不会调用父类init方法 会导致config为空
		super.init(config);
		ServletContext context = config.getServletContext();
		System.out.println(context);
		context.setAttribute("key", "值");
		
		
		
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Context.getRealPath("a.txt"); 该方法能够获取项目的绝对路径 参数能够尾部进行追加/  （wtpwebapps/09_servlet/a.txt）
		ServletContext Context = this.getServletContext();
		String path = Context.getRealPath("a.txt");
//		System.out.println(path);
		
		//通过类加载器获取字节码路径 加载的src下的 
		String path2 = One_Servlet.class.getClassLoader().getResource("a.txt").getPath();
//		System.out.println(path2);
	
	}

}
