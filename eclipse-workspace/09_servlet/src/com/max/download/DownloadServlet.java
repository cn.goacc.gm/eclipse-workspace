package com.max.download;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.misc.BASE64Encoder;

@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("filename");
		System.out.println(name);
		
		String path = this.getServletContext().getRealPath("Download/"+name);
		//根据文件名来获取mime类型
		String type = this.getServletContext().getMimeType(path);
		//设置mimeType
		response.setContentType(type);
		
		/*********解决附件名称乱码问题**********/
		// 获取客户端信息
		String agent = request.getHeader("User-Agent");
		// 定义一个变量记录编码之后的名字
		String filenameEncoder = "";
		if (agent.contains("MSIE")) {
			// IE编码
			filenameEncoder = URLEncoder.encode(name, "utf-8");
			filenameEncoder = filenameEncoder.replace("+", " ");
		} else if (agent.contains("Firefox")) {
			// 火狐编码
			BASE64Encoder base64Encoder = new BASE64Encoder();
			filenameEncoder = "=?utf-8?B?" + base64Encoder.encode(name.getBytes("utf-8")) + "?=";
		} else {
			// 浏览器编码
			filenameEncoder = URLEncoder.encode(name, "utf-8");
		}
		
		//设置以附件形式下载
		response.setHeader("Content-Disposition", "attachment;filename="+filenameEncoder);
		
		ServletOutputStream out = response.getOutputStream();
		FileInputStream in = new FileInputStream(path);
		byte[] buffer=new byte[1024];
		int len=0;
		while((len = in.read(buffer))!=-1) {
			out.write(buffer, 0, len);
		}
		
		in.close();
		
	
	}

}
