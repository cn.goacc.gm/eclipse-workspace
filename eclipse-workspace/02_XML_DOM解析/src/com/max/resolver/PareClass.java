package com.max.resolver;

import java.io.IOException;
import java.util.Scanner;

import javax.xml.crypto.dsig.Transform;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PareClass {
	public static void main(String[] args) throws Exception{
		//创建解析器工厂
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//通过解析工厂得到解析器
		DocumentBuilder db = dbf.newDocumentBuilder();
		//通过解析器得到dcument对象
		Document doc = db.parse("src/com/max/xml/student.xml"); 
		test5(doc);
	}
	//标签添加属性
	static void test5(Document doc) throws Exception {
		//获取要添加属性的结点
		Node item = doc.getElementsByTagName("name").item(0);
		//强制转换成Element
		Element el=(Element) item;
		//设置属性
//		el.removeAttribute("ID"); 删除属性
//		el.setAttribute("ID", "003"); 修改一样
//		el.getTagName();  获取标签名
		el.setAttribute("ID", "001");
		//同步修改
			//创建传输器工厂
			TransformerFactory tf = TransformerFactory.newInstance();
			//得到传输器
			Transformer tfm = tf.newTransformer();
			//修改
			tfm.transform(new DOMSource(doc), new StreamResult("src/com/max/xml/student.xml"));
	}
	
	//删除一个标签（结点）
	static void test4(Document doc) throws Exception {
		//获取删除结点
		Node del_item = doc.getElementsByTagName("address").item(0);
		//通过要删除的结点获取它的父结点
		del_item.getParentNode().removeChild(del_item);
		//同步修改
			//创建传输器工厂
			TransformerFactory tf = TransformerFactory.newInstance();
			//得到传输器
			Transformer tfm = tf.newTransformer();
			//修改
			tfm.transform(new DOMSource(doc), new StreamResult("src/com/max/xml/student.xml"));
		
	}
	
	//增加一个结点
	static void test3(Document doc) throws Exception {
		//创建一个结点
		Element et = doc.createElement("address");
		//设置结点内容
		et.setTextContent("地址1");
		//获取父结点
		Node item = doc.getElementsByTagName("student").item(0);
		//追加结点
		item.appendChild(et);
		//同步修改
			//创建传输器工厂
			TransformerFactory tf = TransformerFactory.newInstance();
			//得到传输器
			Transformer tfm = tf.newTransformer();
			//修改
			tfm.transform(new DOMSource(doc), new StreamResult("src/com/max/xml/student.xml"));
	}
	
	//修改结点内容
	static void test2(Document doc) throws Exception {
		NodeList list = doc.getElementsByTagName("age");
		Node node=list.item(0);
		node.setTextContent("365");
		//内存写到文档的同步操作
		//创建传输器工厂
		TransformerFactory tf = TransformerFactory.newInstance();
		//得到传输器
		Transformer tfm = tf.newTransformer();
		//修改
		tfm.transform(new DOMSource(doc), new StreamResult("src/com/max/xml/student.xml"));
		
	}
	
	//获取具体的结点内容
	static void test1(Document doc) {
		NodeList list = doc.getElementsByTagName("name");  //NodeList列表
		for(int i=0;i<list.getLength();i++) {
			System.out.println(list.item(i).getTextContent());
		}
	}
	
	
	
	
	
	
}
