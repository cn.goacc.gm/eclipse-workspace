package com.max.jdbc.handler;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BeanListHandler<T> implements IResultSethandler<List<T>>{
	
	private Class<T> classType;

	public BeanListHandler(Class<T> classType) {
		this.classType = classType;
	}

	@Override
	public List<T> handler(ResultSet rs) throws Exception {
		List<T> list=new ArrayList<>();
		while (rs.next()) {
			// 创建一个对象
			T obj = this.classType.newInstance();
			// 通过内省拿到属性
			BeanInfo info = Introspector.getBeanInfo(this.classType, Object.class);
			// 获取所有属性描述器
			PropertyDescriptor[] pds = info.getPropertyDescriptors();
			// 遍历
			for (PropertyDescriptor pd : pds) {
				// 获取属性名
				Object vla = rs.getObject(pd.getName());
				// 获取set方法 并赋值
				pd.getWriteMethod().invoke(obj, vla);
			} 
			//添加到列表里
			list.add(obj);
		}
		return list;
	}

}
