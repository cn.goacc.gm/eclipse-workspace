package com.max.jdbc.handler;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.ResultSet;

//通用结果集处理器

public class BeanHandler<T> implements IResultSethandler<T> {

	private Class<T> classType;

	public BeanHandler(Class<T> classType) {
		super();
		this.classType = classType;
	}

	@Override
	public T handler(ResultSet rs) throws Exception {

		if (rs.next()) {
			// 创建一个对象
			T obj = this.classType.newInstance();
			// 通过内省拿到属性
			BeanInfo info = Introspector.getBeanInfo(this.classType, Object.class);
			// 获取所有属性描述器
			PropertyDescriptor[] pds = info.getPropertyDescriptors();
			// 遍历
			for (PropertyDescriptor pd : pds) {
				// 获取属性名
				Object vla = rs.getObject(pd.getName());
				// 获取set方法 并赋值
				pd.getWriteMethod().invoke(obj, vla);
			}
			return obj;
		}

		return null;
	}

}
