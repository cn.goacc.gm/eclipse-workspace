package com.max.jdbc.dao.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.max.jdbc.dao.IStudentDao;
import com.max.jdbc.domain.Student;
import com.max.jdbc.handler.BeanHandler;
import com.max.jdbc.handler.BeanListHandler;
import com.max.jdbc.handler.IResultSethandler;
import com.max.jdbc.util.CRUDTemplate;

public class StudentDaoImpl implements IStudentDao {

	@Override
	public void save(Student stu) {
		String sql = "insert into student (name,age) values(?,?)";
		CRUDTemplate.executeUpdate(sql, stu.getName(), stu.getAge());
	}

	@Override
	public void update(int id, Student stu) {
		String sql = "update student set name=?,age=? where id=?";
		CRUDTemplate.executeUpdate(sql, stu.getName(), stu.getAge(), id);
	}

	@Override
	public void delete(int id) {
		String sql = "delete from student where id=?";
		CRUDTemplate.executeUpdate(sql, id);
	}

	@Override
	public Student get(int id) {
		String sql = "select * from student where id=?";
		return CRUDTemplate.executeQuery(sql, new BeanHandler<Student>(Student.class), id);
	}

	@Override
	public List<Student> getAll() {
		String sql = "select * from student";
		return CRUDTemplate.executeQuery(sql, new BeanListHandler<Student>(Student.class)); 
	}
	
	@Override
	public Integer getCount() {
		String sql="select count(*) as count from student";
		return CRUDTemplate.executeQuery(sql, new StuCountResultSetImp());
	}
	
	//实现Student结果集的处理
	class StuResultSetHandImp implements IResultSethandler<ArrayList<Student>>{
		@Override
		public ArrayList<Student> handler(ResultSet rs) throws Exception {
			ArrayList<Student> list=new ArrayList<Student>();
			while (rs.next()) {
				Student stu = new Student();
				stu.setId(rs.getInt("id"));
				stu.setName(rs.getString("name"));
				stu.setAge(rs.getInt("age"));
				list.add(stu);
			}
			return list;
		}
		
	}
	
	//计数结果集的处理
	class StuCountResultSetImp implements IResultSethandler<Integer>{	//注意泛型不能返回基本数据类型

		@Override
		public Integer handler(ResultSet rs) throws Exception {
			if(rs.next()) {
				return rs.getInt("count");
			}
			return null;
		}
		
	}

	
	
	
	
	
	
	

}
