package com.max.jdbc.dao.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.max.jdbc.dao.IUserDao;
import com.max.jdbc.domain.User;
import com.max.jdbc.handler.IResultSethandler;
import com.max.jdbc.util.CRUDTemplate;

public class UserDaoimpl implements IUserDao{

	@Override
	public List<User> getAll() {
		String sql="select * from user";
		ArrayList<User> list = CRUDTemplate.executeQuery(sql,new UserResultSetHandImp());
		
		return list;
	}
	class UserResultSetHandImp implements IResultSethandler<ArrayList<User>>{

		@Override
		public ArrayList<User> handler(ResultSet rs) throws Exception {
			ArrayList<User> list =new ArrayList<User>();
			while(rs.next()) {
				User user=new User();
				user.setName(rs.getString("name"));
				user.setPwd(rs.getString("psd"));
				list.add(user);
			}
			
			return list;
		}
		
	}

}
