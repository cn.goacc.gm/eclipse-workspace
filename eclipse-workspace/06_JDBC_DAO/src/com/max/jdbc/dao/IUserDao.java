package com.max.jdbc.dao;

import java.util.List;

import com.max.jdbc.domain.User;

public interface IUserDao {

	List<User> getAll();
}
