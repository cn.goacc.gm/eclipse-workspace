package com.max.jdbc.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.max.jdbc.handler.IResultSethandler;

public class CRUDTemplate {

	
	public static int executeUpdate(String sql, Object... params) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = JDBCUtil.getconn();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				ps.setObject(i + 1, params[i]); // 注意⚠️ 从1开始
			} 
			return ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, null);
		}
		return 0;
	}

	public static <T>T executeQuery(String sql, IResultSethandler<T> rh, Object... params) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = JDBCUtil.getconn();
			ps = conn.prepareStatement(sql);
			for (int i = 0; i < params.length; i++) {
				ps.setObject(i + 1, params[i]);
			}
			rs = ps.executeQuery();
			return rh.handler(rs); // ResultSet结果集处理
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.Close(conn, ps, rs);
		}
		return null;

	}

}
