package com.max.jdbc.test;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

import com.max.jdbc.domain.User;

class ClassTest {
	public Class c;
	public ClassTest(Class c){
		this.c=c;
	}
}

//内省
public class Test {
	public static void main(String[] args) throws Exception {
		/*ClassTest ct = new ClassTest(User.class);
		//通过字节码获取对象
		User u = (User) ct.c.newInstance();*/
		User u = User.class.newInstance();  //直接通字节码创建对象
		//获取指定字节码的信息  包括父类信息，但不包括指定类(Object)
		BeanInfo info = Introspector.getBeanInfo(User.class,Object.class);
		//获取所有属性描述器
		PropertyDescriptor[] pds = info.getPropertyDescriptors();
		for (PropertyDescriptor pd : pds) {
			//获取所有属性名称
			System.out.println(pd.getName());
			//set方法
			System.out.println(pd.getWriteMethod());
			//get方法
			System.out.println(pd.getReadMethod());
			//通过获取set方法设置值
			pd.getWriteMethod().invoke(u, "1111");
		}
		System.out.println(u.toString());
		
		
		
		
	}
}
