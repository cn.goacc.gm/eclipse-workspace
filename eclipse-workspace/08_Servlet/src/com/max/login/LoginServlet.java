package com.max.login;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.max.domain.User;
import com.max.utils.JDBCUtil;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取请求参数 
		String name = request.getParameter("username");
		String pwd = request.getParameter("pwd");
		System.out.println("账号"+name+","+"密码"+pwd);
		QueryRunner qr = new QueryRunner(JDBCUtil.getDataSource());
		String sql="select * from user where name=? and pwd=?";
		User user=null;
		try {
			user = qr.query(sql,new BeanHandler<User>(User.class),name,pwd);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(user!=null) {
			response.getWriter().write("1");
		}else {
			response.getWriter().write("0");
		}
		
		
	}

}
